from torch import nn
import torch
from torch.nn.init import trunc_normal_
from .attention import PositionalEncoding, TransformerEncoder


class TemporalTransformer(nn.Module):
    def __init__(self, input_dim, model_dim, num_classes=2, num_heads=4, num_layers=4, dropout=0.0, input_dropout=0.0):
        """
        Inputs:
            input_dim - Hidden dimensionality of the input
            model_dim - Hidden dimensionality to use inside the Transformer
            num_classes - Number of classes to predict per sequence element
            num_heads - Number of heads to use in the Multi-Head Attention blocks
            num_layers - Number of encoder blocks to use.
            lr - Learning rate in the optimizer
            warmup - Number of warmup steps. Usually between 50 and 500
            max_iters - Number of maximum iterations the model is trained for. This is needed for the CosineWarmup scheduler
            dropout - Dropout to apply inside the model
            input_dropout - Dropout to apply on the input features
        """

        super(TemporalTransformer, self).__init__()
        # self.seq_len = input_dim[1]
        # Input dim -> Model dim
        self.input_net = nn.Sequential(
            nn.Dropout(input_dropout),
            nn.Linear(input_dim, model_dim)
        )
        # Positional encoding for sequences
        self.positional_encoding = PositionalEncoding(d_model=model_dim)
        # Transformer
        self.transformer = TransformerEncoder(num_layers=num_layers,
                                              input_dim=model_dim,
                                              dim_feedforward=2 * model_dim,
                                              num_heads=num_heads,
                                              dropout=dropout)
        # Output classifier per sequence lement
        # self.output_net = nn.Sequential(
        #     nn.Linear(model_dim, model_dim),
        #     nn.LayerNorm(model_dim),
        #     nn.ReLU(inplace=True),
        #     nn.Dropout(dropout),
        #     nn.Linear(model_dim, num_classes)
        # )

    def forward(self, x, mask=None, add_positional_encoding=True):
        """
        Inputs:
            x - Input features of shape [Batch, SeqLen, input_dim]
            mask - Mask to apply on the attention outputs (optional)
            add_positional_encoding - If True, we add the positional encoding to the input.
                                      Might not be desired for some tasks.
        """
        x = self.input_net(x)
        if add_positional_encoding:
            x = self.positional_encoding(x)
        x = self.transformer(x, mask=mask)
        x = x[:, (x.shape[1]-1)//2, :]
        # print("transformer output shape: {}.".format(x.shape))
        # x = self.output_net(x)
        return x
