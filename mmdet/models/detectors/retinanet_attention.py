from mmdet.models.detectors.temporal_transformer import TemporalTransformer
from mmdet.models.builder import DETECTORS
from mmdet.models.detectors.temporal import TemporalDetector


@DETECTORS.register_module()
class RetinaNet_attention(TemporalDetector):
    """Implementation of `RetinaNet <https://arxiv.org/abs/1708.02002>`_"""

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        super(RetinaNet_attention, self).__init__(backbone, neck, None, bbox_head, train_cfg, test_cfg, pretrained)
        self.temporal = TemporalTransformer(input_dim=neck.out_channels, model_dim=neck.out_channels, num_heads=1, num_layers=4)

    def extract_feat(self, images):
        """Directly extract features from the backbone+neck."""
        '''squeeze temporal and channel axis into one axis for training purposes'''
        squeezed_image = images.view(-1, *images.shape[2:])
        features = self.backbone(squeezed_image)

        if self.with_neck:
            features = self.neck(features)

        # '''temporal'''
        in_features = list(features)
        T = images.shape[1]
        out_features = []
        for feature_idx, feature in enumerate(in_features):
            # unsqueeze feature
            BT, C, H, W = feature.shape
            feature = feature.view(BT//T, T, C, H, W)
            # TODO: padding
            feature = feature.permute(0, 3, 4, 1, 2)
            feature = feature.reshape(-1, T, C)  # BHWxTxC
            # feature = torch.unsqueeze(feature, 2)
            # in_feature = feature.view(*images.shape[:2], -1)  # BxTxCHW
            '''apply temporal to each layer feature'''
            out_feature = self.temporal(feature)
            out_feature = out_feature.view(BT//T, H, W, C).permute(0, 3, 1, 2)
            out_features.append(out_feature)

        return out_features
