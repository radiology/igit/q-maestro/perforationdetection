import torch
from torch import nn

from ..builder import DETECTORS
from .temporal import TemporalDetector


@DETECTORS.register_module()
class RetinaNet_T11(TemporalDetector):
    """Implementation of `RetinaNet <https://arxiv.org/abs/1708.02002>`_"""

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 seq_len,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        print("debug: seq_len = {}".format(seq_len))
        super(RetinaNet_T11, self).__init__(backbone, neck, None, bbox_head, train_cfg, test_cfg, pretrained)
        self.temporal = nn.Conv3d(neck.out_channels, neck.out_channels, kernel_size=(seq_len, 1, 1), padding=0)

    def extract_feat(self, images):
        """Directly extract features from the backbone+neck."""
        '''squeeze temporal and channel axis into one axis for training purposes'''
        squeezed_image = images.view(-1, *images.shape[2:])
        features = self.backbone(squeezed_image)

        if self.with_neck:
            features = self.neck(features)

        '''temporal'''
        in_features = list(features)
        out_features = []
        for feature_idx, feature in enumerate(in_features):
            # un-squeeze batch and seq dim axes
            in_feature = feature.view(*images.shape[:2], *feature.shape[1:])
            # swap number of output filters channel and the temporal channel.
            in_feature = in_feature.permute(0, 2, 1, 3, 4)
            '''apply temporal to each layer feature'''
            out_feature = self.temporal(in_feature)
            out_feature = torch.squeeze(out_feature, 2)
            out_features.append(out_feature)

        return out_features


@DETECTORS.register_module()
class RetinaNet_T33(RetinaNet_T11):
    """Implementation of `RetinaNet <https://arxiv.org/abs/1708.02002>`_"""

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 seq_len,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        super(RetinaNet_T33, self).__init__(backbone, neck, bbox_head, seq_len, train_cfg, test_cfg, pretrained)
        self.temporal = nn.Conv3d(neck.out_channels, neck.out_channels, kernel_size=(seq_len, 3, 3), padding=0)


@DETECTORS.register_module()
class RetinaNet_T55(RetinaNet_T11):
    """Implementation of `RetinaNet <https://arxiv.org/abs/1708.02002>`_"""

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 seq_len,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        super(RetinaNet_T55, self).__init__(backbone, neck, bbox_head, seq_len, train_cfg, test_cfg, pretrained)
        self.temporal = nn.Conv3d(neck.out_channels, neck.out_channels, kernel_size=(seq_len, 5, 5), padding=0)
