import torch

from ..builder import DETECTORS
from .temporal import TemporalDetector
from .convgru import ConvGRU, ConvBGRU


@DETECTORS.register_module()
class RetinaNetGRU(TemporalDetector):
    """Implementation of `RetinaNet <https://arxiv.org/abs/1708.02002>`_"""

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        super(RetinaNetGRU, self).__init__(backbone, neck, ConvGRU, bbox_head, train_cfg,
                                           test_cfg, pretrained)


@DETECTORS.register_module()
class RetinaNetBGRU(TemporalDetector):
    """Implementation of `RetinaNet <https://arxiv.org/abs/1708.02002>`_"""

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        super(RetinaNetBGRU, self).__init__(backbone, neck, ConvBGRU, bbox_head, train_cfg,
                                            test_cfg, pretrained)

    def extract_feat(self, images):
        """Directly extract features from the backbone+neck."""
        '''squeeze temporal and channel axis into one axis for training purposes'''
        squeezed_image = images.view(-1, *images.shape[2:])
        features = self.backbone(squeezed_image)

        if self.with_neck:
            features = self.neck(features)

        # '''BiGRU'''
        in_features = list(features)
        seq_len = images.shape[1]
        out_features = []
        for feature_idx, feature in enumerate(in_features):
            # un-squeeze batch and seq dim axes
            in_feature = feature.view(*images.shape[:2], *feature.shape[1:])
            # print("debug: bidirectional gru input feature shape: {}".format(in_feature.shape))
            reversed_in_feature = torch.flip(in_feature, [1])  # expecting BxTxCxHxW
            '''apply gru to each layer feature'''
            gru_out_feature = self.temporal(in_feature, reversed_in_feature)
            # print("debug: bidirectional gru output feature shape: {}".format(gru_out_feature.shape))
            out_feature = gru_out_feature[:, (seq_len - 1) // 2, :, :, :]
            out_features.append(out_feature)

        return out_features
