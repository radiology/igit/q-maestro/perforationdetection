# PerforationDetection
This is the source code of the work titled "Spatio-temporal deep learning for automatic detection of intracranial vessel perforation in digital subtraction angiography during endovascular thrombectomy".

***
## Contact
For questions, feel free to contact me via r dot su at erasmusmc.nl.

This implementation is written based on MMDetection. For more details about MMDetection, please refer to https://github.com/open-mmlab/mmdetection.
