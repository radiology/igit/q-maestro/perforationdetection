import argparse
import glob
import logging
import os
import sys
from pathlib import Path
from tools.model_ensemble import ensemble_learning
import mmcv
import numpy as np
from scipy.stats import ttest_ind, f_oneway
from tools.compare_auc_delong_xu import delong_roc_test

logger = logging.getLogger(__name__)


def t_test(x, y, alternative='both-sided'):
    _, double_p = ttest_ind(x, y, equal_var=False)
    if alternative == 'both-sided':
        pval = double_p
    elif alternative == 'greater':
        if np.mean(x) > np.mean(y):
            pval = double_p / 2.
        else:
            pval = 1.0 - double_p / 2.
    elif alternative == 'less':
        if np.mean(x) < np.mean(y):
            pval = double_p / 2.
        else:
            pval = 1.0 - double_p / 2.
    else:
        raise ValueError("Invalid parameter.")
    return pval


def get_seq_cls_prob_errors(model_name, seq_len, args, use_so_results):
    seq_y_true = []
    seq_y_score = []
    run_dirpaths = sorted(glob.glob(os.path.join(args.dir, 'run[0-9]/'), recursive=False))
    logger.info("Gather results of all {} runs of the experiment. "
                "Model: {}, seq_len: {}".format(len(run_dirpaths), model_name, seq_len))
    for run_dirpath in run_dirpaths:
        if model_name != 'ensemble':
            result_dirname = "{}_seqlen{}_cosinelr5e-04".format(model_name, seq_len)
            result_filename = "{}_result_dicts_max{}.pkl".format(('overall', 'optimized')[use_so_results], args.max_per_img)
            model_result_filepath = os.path.join(run_dirpath, result_dirname, result_filename)
            model_result_dicts = mmcv.load(model_result_filepath)
            for i, d in enumerate(model_result_dicts):
                pos = int(np.argmax(model_result_dicts[i]['scores'], axis=0)) if model_result_dicts[i]['scores'] else None
                model_result_dicts[i]['scores'] = model_result_dicts[i]['scores'][pos] if pos is not None else None
                model_result_dicts[i]['pd_loc'] = model_result_dicts[i]['pd_loc'][pos] if pos is not None else None

            sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in model_result_dicts])
            # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in model_result_dicts_list[0]])
            for s in sequences:
                frames = [d for d in model_result_dicts if s in d['file_name']]
                seq_y_true.append(any([(f['gt_loc'] is not None) for f in frames]))
                seq_y_score.append(max([0] + [f['scores'] for f in frames if f['pd_loc'] is not None]))
        else:
            _args = parse_args()
            vars(_args)['run_dir'] = run_dirpath
            vars(_args)['models'] = [
                ('retinanet_cosine12_bilstm', 5),
                ('retinanet_cosine12_bigru', 5),
                ('retinanet_cosine12_t11', 5),
                ('retinanet_cosine12_att', 5)]
            t, s = ensemble_learning(_args, use_so_results, return_scores_only=True)
            seq_y_score.extend(s)
            seq_y_true.extend(t)

    seq_y_error = [abs(a - b) for a, b in zip(seq_y_true, seq_y_score)]
    return seq_y_error


def get_seq_cls_probs(model_name, seq_len, args, use_so_results):
    seq_y_true = []
    seq_y_score = []
    run_dirpaths = sorted(glob.glob(os.path.join(args.dir, 'run[0-9]/'), recursive=False))
    logger.info("Gather results of all {} runs of the experiment. "
                "Model: {}, seq_len: {}".format(len(run_dirpaths), model_name, seq_len))
    for run_dirpath in run_dirpaths:
        if model_name != 'ensemble':
            result_dirname = "{}_seqlen{}_cosinelr5e-04".format(model_name, seq_len)
            result_filename = "{}_result_dicts_max{}.pkl".format(('overall', 'optimized')[use_so_results], args.max_per_img)
            model_result_filepath = os.path.join(run_dirpath, result_dirname, result_filename)
            model_result_dicts = mmcv.load(model_result_filepath)
            for i, d in enumerate(model_result_dicts):
                pos = int(np.argmax(model_result_dicts[i]['scores'], axis=0)) if model_result_dicts[i]['scores'] else None
                model_result_dicts[i]['scores'] = model_result_dicts[i]['scores'][pos] if pos is not None else None
                model_result_dicts[i]['pd_loc'] = model_result_dicts[i]['pd_loc'][pos] if pos is not None else None

            sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in model_result_dicts])
            # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in model_result_dicts_list[0]])
            for s in sequences:
                frames = [d for d in model_result_dicts if s in d['file_name']]
                seq_y_true.append(any([(f['gt_loc'] is not None) for f in frames]))
                seq_y_score.append(max([0] + [f['scores'] for f in frames if f['pd_loc'] is not None]))
        else:
            _args = parse_args()
            vars(_args)['run_dir'] = run_dirpath
            vars(_args)['models'] = [
                ('retinanet_cosine12_bilstm', 5),
                ('retinanet_cosine12_bigru', 5),
                ('retinanet_cosine12_t11', 5),
                ('retinanet_cosine12_att', 5)]
            t, s = ensemble_learning(_args, use_so_results, return_scores_only=True)
            seq_y_score.extend(s)
            seq_y_true.extend(t)

    return seq_y_score, seq_y_true


def compare_models(args, use_so_results):
    model_seq_cls_prob_errors_list = []
    for model_name, seq_len in zip(args.models, args.seq_lens):
        model_seq_cls_prob_errors_list.append(get_seq_cls_prob_errors(model_name, seq_len, args, use_so_results))
    if len(model_seq_cls_prob_errors_list) == 2:
        logger.info("Performing one tailed paired T test")
        p_value = t_test(model_seq_cls_prob_errors_list[0], model_seq_cls_prob_errors_list[1], alternative='less')
    else:
        _, p_value = f_oneway(*model_seq_cls_prob_errors_list)
        logger.info("Performing one-way anova test")
    logger.info("P-value: {}".format(p_value))


def compare_model_AUC(args):
    model_seq_cls_prob_list = []
    seq_y_true = []
    for model_name, seq_len, use_so_results in zip(args.models, args.seq_lens, args.use_so_results):
        seq_y_score, seq_y_true = get_seq_cls_probs(model_name, seq_len, args, use_so_results)
        model_seq_cls_prob_list.append(seq_y_score)
    logger.info("Performing DeLong Test to calculate the significance of AUC difference between two models.")
    p_value = delong_roc_test(np.array(seq_y_true, dtype=int), model_seq_cls_prob_list[0], model_seq_cls_prob_list[1])
    logger.info("p-value = {}".format(p_value))


def parse_args():
    """
    Argument parser for the main function
    """
    parser = argparse.ArgumentParser(description='Check if model 1 is significantly better than model 2 in terms of '
                                                 'series level classification')
    parser.add_argument('--models', type=str, nargs='+', help='name of models')
    parser.add_argument('--use_so_results', type=int, nargs='+', help='Whether to take SO results for the model.')
    parser.add_argument('--seq_lens', type=int, nargs='+', help='seq_lens of models')
    parser.add_argument('--dir', type=str, help='The input dir containing runs of cv results of various models.')
    parser.add_argument('--max_per_img', type=int, default=1, help='Max number of detections per image in test')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    log_filepath = '{}/main_compare_{}.log' \
                   ''.format(args.dir, '_'.join(['{}_s{}'.format(a, b) for a, b in zip(args.models, args.seq_lens)]))
    Path(log_filepath).parent.mkdir(parents=True, exist_ok=True)
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.FileHandler(log_filepath, mode='w'),
                                  logging.StreamHandler(sys.stdout)])

    logger.info("Argument list: {}".format(sys.argv))
    compare_model_AUC(args)
    print("Done!")
