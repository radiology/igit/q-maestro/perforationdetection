import argparse
import glob
import logging
import os
import random
import subprocess
import sys
from datetime import datetime
from pathlib import Path
import json
import mmcv
import pandas as pd
import torch
from natsort import natsorted

from eval_utils import generate_subset_json, evaluate, evaluate2

logger = logging.getLogger(__name__)


def generate_coco_json_for_generalizability(in_dir, out_json_dir):
    Path(out_json_dir).mkdir(parents=True, exist_ok=True)

    logger.info("======= Statistics: all =======")
    all_images = natsorted(glob.glob(os.path.join(in_dir, '**', '*.jpg'), recursive=True))
    perf_images = natsorted(glob.glob(os.path.join(in_dir, 'perforation', '**', '*.jpg'), recursive=True))
    non_perf_images = natsorted(glob.glob(os.path.join(in_dir, 'non_perforation', '**', '*.jpg'), recursive=True))
    all_seqs = set([os.path.basename(os.path.dirname(f)) for f in all_images])
    perf_seqs = set([os.path.basename(os.path.dirname(f)) for f in perf_images])
    non_perf_seqs = set([os.path.basename(os.path.dirname(f)) for f in non_perf_images])
    perf_patients = set([os.path.basename(Path(f).parent.parent) for f in perf_images])
    non_perf_patients = set([os.path.basename(Path(f).parent.parent) for f in non_perf_images])
    all_patients = set([os.path.basename(Path(f).parent.parent) for f in all_images])
    logger.info("Images: perf ({}) + nonperf ({}) = {}".format(len(perf_images), len(non_perf_images), len(all_images)))
    logger.info("Sequences: perf ({}) + nonperf ({}) = {}".format(len(perf_seqs), len(non_perf_seqs), len(all_seqs)))
    logger.info("Patients: perf ({}) + nonperf ({}) = {}".format(len(perf_patients), len(non_perf_patients), len(all_patients)))

    """Per dataset info"""
    mrclean_images = glob.glob(os.path.join(in_dir, '**', 'R*', '**', '*.jpg'), recursive=True)
    noiv_images = glob.glob(os.path.join(in_dir, '**', 'MRCLEANNOIV*', '**', '*.jpg'), recursive=True)
    hermes_images = glob.glob(os.path.join(in_dir, '**', 'HA*', '**', '*.jpg'), recursive=True)

    mrclean_perf_images = [f for f in mrclean_images if os.path.basename(Path(f).parent.parent) in perf_patients]
    mrclean_non_perf_images = [f for f in mrclean_images if os.path.basename(Path(f).parent.parent) not in perf_patients]

    hermes_perf_images = [f for f in hermes_images if os.path.basename(Path(f).parent.parent) in perf_patients]
    hermes_perf_patients = natsorted(list(set([os.path.basename(Path(f).parent.parent) for f in hermes_perf_images])))
    noiv_perf_images = [f for f in noiv_images if os.path.basename(Path(f).parent.parent) in perf_patients]
    noiv_perf_patients = natsorted(list(set([os.path.basename(Path(f).parent.parent) for f in noiv_perf_images])))

    hermes_non_perf_images = [f for f in hermes_images if os.path.basename(Path(f).parent.parent) not in perf_patients]
    hermes_non_perf_patients = natsorted(list(set([os.path.basename(Path(f).parent.parent) for f in hermes_non_perf_images])))
    noiv_non_perf_images = [f for f in noiv_images if os.path.basename(Path(f).parent.parent) not in perf_patients]
    noiv_non_perf_patients = natsorted(list(set([os.path.basename(Path(f).parent.parent) for f in noiv_non_perf_images])))

    logger.info("======= Statistics: test set =======")
    test_images = natsorted(hermes_images+noiv_images)
    test_perf_images = [f for f in test_images if os.path.basename(Path(f).parent.parent) in perf_patients]
    test_non_perf_images = [f for f in test_images if os.path.basename(Path(f).parent.parent) not in perf_patients]
    logger.info("Generating json for generalizability test data (both perf and non-perf images)")
    generate_subset_json(all_images, test_images, os.path.join(out_json_dir, 'test.json'))
    logger.info("Generating json for generalizability test data (only perf images)")
    generate_subset_json(all_images, test_perf_images, os.path.join(out_json_dir, 'perf_test.json'))
    logger.info("Generating json for generalizability test data (only non_perf images)")
    generate_subset_json(all_images, test_non_perf_images, os.path.join(out_json_dir, 'non_perf_test.json'))

    logger.info("======= Statistics: training set =======")
    train_perf_images = natsorted(mrclean_perf_images)
    train_non_perf_images = natsorted(mrclean_non_perf_images)
    train_images = mrclean_images
    logger.info("Generating json for generalizability training data (both perf and non-perf images)")
    generate_subset_json(all_images, train_images, os.path.join(out_json_dir, 'train.json'))
    logger.info("Generating json for generalizability training data (only perf images)")
    generate_subset_json(all_images, train_perf_images, os.path.join(out_json_dir, 'perf_train.json'))
    logger.info("Generating json for generalizability training data (only non_perf images)")
    generate_subset_json(all_images, train_non_perf_images, os.path.join(out_json_dir, 'non_perf_train.json'))

    test_perf_subsets = [noiv_perf_patients, hermes_perf_patients]
    test_non_perf_subsets = [noiv_non_perf_patients, hermes_non_perf_patients]
    logger.info("======= Statistics: Two fold training and testing =======")
    for fold in range(2):
        logger.info("================= Fold {} ==================".format(fold))
        fold_test_perf_images = natsorted(
            [f for f in perf_images if os.path.basename(Path(f).parent.parent) in test_perf_subsets[fold]])
        fold_test_non_perf_images = natsorted(
            [f for f in non_perf_images if os.path.basename(Path(f).parent.parent) in test_non_perf_subsets[fold]])
        fold_test_images = natsorted(fold_test_perf_images + fold_test_non_perf_images)
        logger.info("Generating json for generalizability test data (perf + non-perf) of fold {}".format(fold))
        generate_subset_json(all_images, fold_test_images, os.path.join(out_json_dir, 'fold{}_test.json'.format(fold)))
        logger.info("Generating json for test data (only perf images) of fold {}".format(fold))
        generate_subset_json(all_images, fold_test_perf_images, os.path.join(out_json_dir, 'fold{}_perf_test.json'.format(fold)))
        logger.info("Generating json for test data (only non-perf images) of fold {}".format(fold))
        generate_subset_json(all_images, fold_test_non_perf_images, os.path.join(out_json_dir, 'fold{}_non_perf_test.json'.format(fold)))

        fold_train_perf_images = [f for f in test_perf_images if os.path.basename(Path(f).parent.parent) not in test_perf_subsets[fold]]
        fold_train_perf_images.extend(train_perf_images)
        fold_train_perf_images = natsorted([f for f in fold_train_perf_images if os.path.isfile(f.replace('images', 'labels').replace('.jpg', '.txt'))])
        fold_train_non_perf_images = [f for f in test_non_perf_images if os.path.basename(Path(f).parent.parent) not in test_non_perf_subsets[fold]]
        fold_train_non_perf_images.extend(train_non_perf_images)
        fold_train_non_perf_images = natsorted(fold_train_non_perf_images)
        fold_train_images = natsorted(fold_train_perf_images + fold_train_non_perf_images)
        logger.info("Generating json for generalizability training data (both perf and non-perf images)")
        generate_subset_json(all_images, fold_train_images, os.path.join(out_json_dir, 'fold{}_train.json'.format(fold)))
        logger.info("Generating json for generalizability training data (only perf images)")
        generate_subset_json(all_images, fold_train_perf_images,
                             os.path.join(out_json_dir, 'fold{}_perf_train.json'.format(fold)))
        logger.info("Generating json for generalizability training data (only non_perf images)")
        generate_subset_json(all_images, fold_train_non_perf_images,
                             os.path.join(out_json_dir, 'fold{}_non_perf_train.json'.format(fold)))


def generalizability_test(args, k=2):
    for i in range(k):
        logger.info("=========== fold {} ==================".format(i))
        """Train the model for fold if trained model not found in output dir."""
        test_json_filepath = os.path.join(args.json_dir, "fold{}_test.json".format(i))

        test_perf_json_filepath = os.path.join(args.json_dir, "fold{}_perf_test.json".format(i))
        test_non_perf_json_filepath = os.path.join(args.json_dir, "fold{}_non_perf_test.json".format(i))
        # train_perf_json_filepath = os.path.join(args.json_dir, "fold{}_perf_train.json".format(i))
        train_non_perf_json_filepath = os.path.join(args.json_dir, "fold{}_non_perf_train.json".format(i))

        train_json_filepath = os.path.join(args.json_dir, "fold{}_train.json".format(i))

        fold_train_out_dir = os.path.join(args.out_dir, 'fold{}'.format(i))
        out_vis_dir = os.path.join(fold_train_out_dir, 'vis')
        Path(out_vis_dir).mkdir(parents=True, exist_ok=True)
        fold_test_out_dir = os.path.join(args.out_dir, 'fold{}'.format(i), "results_max{}".format(args.max_per_img))
        Path(fold_test_out_dir).mkdir(parents=True, exist_ok=True)

        fold_perf_results_pickle_path = os.path.join(fold_test_out_dir, "results_perf.pkl")
        fold_non_perf_results_pickle_path = os.path.join(fold_test_out_dir, "results_non_perf.pkl")
        # fold_results_pickle_path = os.path.join(fold_test_out_dir, "results.pkl")

        fold_perf_result_dicts_pickle_path = os.path.join(fold_test_out_dir, "result_dicts_perf.pkl")
        fold_non_perf_result_dicts_pickle_path = os.path.join(fold_test_out_dir, "result_dicts_non_perf.pkl")
        fold_result_dicts_pickle_path = os.path.join(fold_test_out_dir, "result_dicts.pkl")
        fold_vis_predictions_dir = os.path.join(fold_test_out_dir, "vis_predictions")
        fold_vis_gt_pd_dir = os.path.join(fold_test_out_dir, "vis_gt_pd")

        if not (os.path.isfile(fold_perf_result_dicts_pickle_path) and os.path.isfile(fold_result_dicts_pickle_path) \
                and os.path.isfile(fold_non_perf_result_dicts_pickle_path)):
            cfg_max_per_img = "model.test_cfg.rcnn.max_per_img={}".format(str(args.max_per_img))
            if 'retina' in args.method:
                cfg_max_per_img = "model.test_cfg.max_per_img={}".format(str(args.max_per_img))

            cfg_option_list = [cfg_max_per_img,
                               "optimizer.lr={}".format(args.lr),
                               "data.train.ann_file={}".format(train_json_filepath),
                               "data.train.seq_len={}".format(args.seq_len),
                               "data.val.seq_len={}".format(args.seq_len),
                               "data.test.seq_len={}".format(args.seq_len),
                               "data.train.filter_empty_gt={}".format(('False', 'True')[args.filter_empty_gt])]
            if 'retinanet1x_t' in args.method or 'retinanet_cosine12_t' in args.method:
                cfg_option_list.append("model.seq_len={}".format(args.seq_len))
            if not os.path.isfile(os.path.join(fold_train_out_dir, "latest.pth")):
                logger.info("===== Training =====")
                subprocess.call(["python", "tools/train.py", args.config_file, "--work-dir", fold_train_out_dir,
                                 "--cfg-options", *cfg_option_list,
                                 "data.val.ann_file={}".format(test_json_filepath),
                                 "data.test.ann_file={}".format(test_json_filepath)])

            logger.info("===== Testing perf test data =====")
            subprocess.call(
                ["python", "tools/test.py", args.config_file, os.path.join(fold_train_out_dir, "latest.pth"),
                 "--cfg-options", *cfg_option_list,
                 "data.val.ann_file={}".format(test_perf_json_filepath),
                 "data.test.ann_file={}".format(test_perf_json_filepath),
                 "--out", fold_perf_results_pickle_path,
                 "--out-dict", fold_perf_result_dicts_pickle_path,
                 # "--show-dir", os.path.join(fold_vis_predictions_dir, "perf"),
                 "--eval", "bbox"])
            # logger.info("===== Visualizing results =====")
            # subprocess.call(["python", "tools/analysis_tools/analyze_results.py", args.config_file,
            #                  fold_perf_results_pickle_path, os.path.join(fold_test_out_dir, 'vis_gt_pd', 'perf'),
            #                  "--wait-time", '0.5', "--cfg-options", cfg_max_per_img,
            #                  "data.test.ann_file={}".format(test_perf_json_filepath)])
            logger.info("===== Testing non perf test data =====")
            subprocess.call(
                ["python", "tools/test.py", args.config_file, os.path.join(fold_train_out_dir, "latest.pth"),
                 "--cfg-options", *cfg_option_list,
                 "data.val.ann_file={}".format(test_non_perf_json_filepath),
                 "data.test.ann_file={}".format(test_non_perf_json_filepath),
                 "--out", fold_non_perf_results_pickle_path,
                 "--out-dict", fold_non_perf_result_dicts_pickle_path,
                 # "--show-dir", os.path.join(fold_vis_predictions_dir, "non_perf"),
                 "--eval", "bbox"])
            # logger.info("===== Visualizing results =====")
            # subprocess.call(["python", "tools/analysis_tools/analyze_results.py", args.config_file,
            #                  fold_non_perf_results_pickle_path, os.path.join(fold_vis_gt_pd_dir, 'non_perf'),
            #                  "--wait-time", '0.5', "--cfg-options", cfg_max_per_img,
            #                  "data.test.ann_file={}".format(test_non_perf_json_filepath)])

        mmcv.dump(mmcv.load(fold_perf_result_dicts_pickle_path) + mmcv.load(fold_non_perf_result_dicts_pickle_path),
                  fold_result_dicts_pickle_path)
        logger.info("Evaluating only perf data")
        evaluate(mmcv.load(fold_perf_result_dicts_pickle_path))
        logger.info("Evaluating both perf and non perf data")
        evaluate(mmcv.load(fold_result_dicts_pickle_path))
        _, _, _, _, _, fold_performance_dict = evaluate2(mmcv.load(fold_result_dicts_pickle_path))
        json.dump(fold_performance_dict,
                  open(os.path.join(args.out_dir, "fold{}".format(i), "results_max{}".format(args.max_per_img), "performance_max{}.json".format(args.max_per_img)), 'w+'))
    logger.info("========== Overall performance evaluation ==============")
    final_results_dict = []
    for fold in range(k):
        fold_test_out_dir = os.path.join(args.out_dir, 'fold{}'.format(fold), "results_max{}".format(args.max_per_img))
        result_pickle_path = os.path.join(fold_test_out_dir, 'result_dicts.pkl')
        final_results_dict.extend(mmcv.load(result_pickle_path))
    overall_results_dict_pickle_path = os.path.join(args.out_dir,
                                                    "overall_result_dicts_max{}.pkl".format(args.max_per_img))
    mmcv.dump(final_results_dict, overall_results_dict_pickle_path)
    evaluate(mmcv.load(overall_results_dict_pickle_path))
    _, _, _, seq_dict_list, perf_dict_list, performance_dict = evaluate2(mmcv.load(overall_results_dict_pickle_path))

    pd.DataFrame(seq_dict_list).to_csv(os.path.join(args.out_dir, "seq_cls_results_max{}.csv".format(args.max_per_img)),
                                       index=False)
    pd.DataFrame(perf_dict_list).to_csv(
        os.path.join(args.out_dir, "perf_det_results_max{}.csv".format(args.max_per_img)), index=False)
    json.dump(performance_dict,
              open(os.path.join(args.out_dir, "performance_max{}.json".format(args.max_per_img)), 'w+'))


def parse_args():
    """
    Argument parser for the main function
    """
    parser = argparse.ArgumentParser(description='Register a pair of images')
    parser.add_argument('--method', type=str, help='The chosen method and model')
    parser.add_argument('--data', type=str, default='data/perforation', help='The input data dir path')
    parser.add_argument('--out_dir', type=str, help='The dir to store all outputs')
    parser.add_argument('--filter_empty_gt', action='store_true', help='Whether to use background images for training')
    parser.add_argument('--max_per_img', type=int, default=1, help='Max number of detections per image in test')
    parser.add_argument('--lr', type=float, default=1e-3, help='Base learning rate')
    parser.add_argument('--seq_len', type=int, default=3, help='Sequence length of data loader for temporal models')

    return parser.parse_args()


if __name__ == '__main__':
    """Train and cross validate various methods on the perforation data"""
    random.seed(42)
    torch.multiprocessing.set_sharing_strategy('file_system')

    args = parse_args()

    MODEL_CONFIG = {
        'retinanet_cosine12': "configs/perforation/retinanet_r50_fpn_cosine12.py",
        'faster_rcnn_cosine12': "configs/perforation/faster_rcnn_r50_fpn_cosine12.py",
        'cascade_rcnn_cosine12': "configs/perforation/cascade_rcnn_r50_fpn_cosine12.py",
        'retinanet_cosine12_bilstm': "configs/perforation/vid_retinanet_r50_fpn_bilstm_cosine12.py",
        'retinanet_cosine12_bigru': "configs/perforation/vid_retinanet_r50_fpn_bigru_cosine12.py",
        'retinanet_cosine12_t11': "configs/perforation/vid_retinanet_r50_fpn_tx1x1_cosine12.py",
        'retinanet_cosine12_att': "configs/perforation/vid_retinanet_r50_fpn_att_cosine12.py",

        'retinanet_cosine12_f2c': "configs/perforation/vid_retinanet_r50_fpn_3frames_cosine12.py",
        'libra_rcnn_cosine12': "configs/perforation/libra_faster_rcnn_r50_fpn_cosine12.py",
        'rpn_cosine12': "configs/perforation/rpn_r50_fpn_cosine12.py",
        'retinanet_cosine12_lstm': "configs/perforation/vid_retinanet_r50_fpn_lstm_cosine12.py",
        'retinanet_cosine12_gru': "configs/perforation/vid_retinanet_r50_fpn_gru_cosine12.py",
        'retinanet_cosine12_t33': "configs/perforation/vid_retinanet_r50_fpn_tx3x3_cosine12.py",
        'retinanet_cosine12_t55': "configs/perforation/vid_retinanet_r50_fpn_tx5x5_cosine12.py",

    }

    folder_name = "{}_seqlen{}_cosinelr{:.0e}".format(args.method, args.seq_len, args.lr)
    out_dir = os.path.join(args.out_dir, folder_name)
    log_filepath = '{}/generalizability_max{}.log'.format(out_dir, args.max_per_img)
    Path(log_filepath).parent.mkdir(parents=True, exist_ok=True)
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.FileHandler(log_filepath, mode='w'),
                                  logging.StreamHandler(sys.stdout)])

    logger.info("Generalizability Argument list: {}".format(sys.argv))
    vars(args)['out_dir'] = out_dir
    vars(args)['json_dir'] = os.path.join(args.data, 'json/generalizability')
    vars(args)['config_file'] = MODEL_CONFIG[args.method]
    # generate_coco_json_for_generalizability(args.data, args.json_dir)
    generalizability_test(args)
    print("Done!")
