import os
import mmcv
from tools.eval_utils import evaluate2
import logging
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
import argparse
import random
import sys
logger = logging.getLogger(__name__)


def collect_results(args):
    results = {}
    so_method_dirname_dict = {'RN+BiLSTM+SO': 'retinanet_cosine12_bilstm_seqlen5_cosinelr5e-04',
                              'RN+BiGRU+SO': 'retinanet_cosine12_bigru_seqlen5_cosinelr5e-04',
                              'RN+TCM+SO': 'retinanet_cosine12_t11_seqlen5_cosinelr5e-04',
                              'RN+TAM+SO': 'retinanet_cosine12_att_seqlen5_cosinelr5e-04'}

    for method, dirname in so_method_dirname_dict.items():
        result_path = os.path.join(args.dir, dirname, 'optimized_result_dicts_max{}.pkl'.format(args.max_per_img))
        final_results_dict = mmcv.load(result_path)
        rows_perf, seq_y, seq_scores, _, _, _ = evaluate2(final_results_dict)
        results[method] = {'perf_apcin': [row[1] for row in rows_perf],
                           'perf_arcin': [row[2] for row in rows_perf],
                           'perf_ap50': [row[7] for row in rows_perf],
                           'perf_ar50': [row[8] for row in rows_perf],
                           'seq_y': seq_y,
                           'seq_scores': seq_scores}

    '''1. Plotting perforation level Precision-Recall curve with the center overlapping metric'''
    plt.figure()
    plt.rc('font', size=15)
    # plt.subplot(131)
    for method, result in results.items():
        plt.plot(result['perf_arcin'], result['perf_apcin'], label=method)

    plt.plot(0.68, 0.96, 's', label='Expert 2')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    # plt.title('Precision-recall curve with the center overlap metric')
    plt.legend(loc='lower left', fancybox=True, framealpha=0, fontsize='x-small')
    plt.tight_layout()
    plt.savefig(os.path.join(args.dir, 'interob_pr_cin.png'), dpi=1200)
    plt.show()
    plt.close()

    '''2. Plotting perforation level Precision-Recall curve with the IOU>=50% metric'''
    plt.figure()
    plt.rc('font', size=15)
    # plt.subplot(132)
    for method, result in results.items():
        plt.plot(result['perf_ar50'], result['perf_ap50'], label=method)
    plt.plot(0.45, 0.63, 's', label='Expert 2')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    # plt.title('Precision-recall curve with the IoU>=50 metric')
    plt.legend(loc='lower left', fancybox=True, framealpha=0, fontsize='x-small')
    plt.tight_layout()
    plt.savefig(os.path.join(args.dir, 'interob_pr_curve_iou50.png'), dpi=1200)
    plt.show()
    plt.close()

    '''3. Plotting acquisition level ROC curve with the center overlapping metric'''
    plt.figure()
    plt.rc('font', size=15)
    # plt.subplot(133)
    lw = 2
    for method, result in results.items():
        fpr, tpr, thresholds = roc_curve(result['seq_y'][0], result['seq_scores'], pos_label=1)
        auc = roc_auc_score(result['seq_y'][0], result['seq_scores'])
        plt.plot(fpr, tpr, lw=lw, label='{} (area = {:.2f})'.format(method, auc))  # color='darkorange',
    plt.plot(0, 0.71, 's', label='Expert 2')
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([-0.05, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    # plt.title('ROC curve for perforation classification on acquisition level')
    plt.legend(loc='lower right', fancybox=True, framealpha=0, fontsize='x-small')
    plt.tight_layout()
    plt.savefig(os.path.join(args.dir, 'interob_series_ROC.png'), dpi=1200)
    plt.show()
    plt.close()


def parse_args():
    """
    Argument parser for the main function
    """
    parser = argparse.ArgumentParser(description='Register a pair of images')
    parser.add_argument('--dir', type=str, help='The dir path containing the result of runs of all models.')
    parser.add_argument('--max_per_img', type=int, default=1, help='Max number of detections per image in test')

    return parser.parse_args()


if __name__ == '__main__':
    """Train and cross validate various methods on the perforation data"""
    random.seed(42)

    args = parse_args()

    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.StreamHandler(sys.stdout)])

    collect_results(args)
    print("Done!")
