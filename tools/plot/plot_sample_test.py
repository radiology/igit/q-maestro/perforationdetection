import os
import mmcv
import numpy as np

from tools.eval_utils import evaluate2
import logging
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
import argparse
import random
import sys
logger = logging.getLogger(__name__)

data_size = [1, 2, 3, 4, 5, 6, 7, 8, 9]
auc =     [0.698, 0.785, 0.813, 0.844, 0.866, 0.869, 0.883, 0.894, 0.900]
auc_std = np.array([0.027, 0.012, 0.010, 0.011, 0.003, 0.012, 0.015, 0.010, 0.008])
mSens95 =     [0.228, 0.336, 0.394, 0.502, 0.542, 0.553, 0.604, 0.624, 0.625]
mSens95_std = np.array([0.060, 0.027, 0.070, 0.043, 0.073, 0.020, 0.021, 0.049, 0.033])

fig = plt.figure()
plt.errorbar(data_size, auc, auc_std, fmt='r*--', label='AUC', capsize=5)
plt.errorbar(data_size, mSens95, mSens95_std, fmt='m*:', label='mSens95', capsize=5)
axes = plt.gca()
for i, j in zip(data_size, auc):
    axes.annotate('{:.3f}'.format(j), xy=(i-0.3, j+0.03), fontsize=13)

for i, j in zip(data_size, mSens95):
    axes.annotate('{:.3f}'.format(j), xy=(i-0.3, j+0.03), fontsize=13)

axes.set_xlim([0.7, 9.6])
axes.set_ylim([0.19, 0.96])

plt.xlabel('train_size/test_size', fontsize=16)
plt.ylabel('score', fontsize=16)
plt.legend(loc='lower right', fancybox=True, framealpha=0, fontsize=16)  # fontsize='x-small')
plt.tight_layout()
fig.savefig('sample_test.png', dpi=1200)

plt.show()


# def collect_results(args):
#     results = {}
#     method_dirname_dict = {'1x': os.path.join(args.dir, '1t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '2x': os.path.join(args.dir, '2t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '3x': os.path.join(args.dir, '3t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '4x': os.path.join(args.dir, '4t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '5x': os.path.join(args.dir, '5t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '6x': os.path.join(args.dir, '6t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '7x': os.path.join(args.dir, '7t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '8x': os.path.join(args.dir, '8t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            '9x': os.path.join(args.dir, '9t/retinanet_cosine12_t11_seqlen5_cosinelr5e-04'),
#                            }
#     # colors = ['b', 'o', 'p']
#
#     for method, dirpath in method_dirname_dict.items():
#         result_path = os.path.join(dirpath, 'overall_result_dicts_max{}.pkl'.format(args.max_per_img))
#         final_results_dict = mmcv.load(result_path)
#         rows_perf, seq_y, seq_scores, _, _, _ = evaluate2(final_results_dict)
#         results[method] = {'perf_apcin': [row[1] for row in rows_perf],
#                            'perf_arcin': [row[2] for row in rows_perf],
#                            'perf_ap50': [row[7] for row in rows_perf],
#                            'perf_ar50': [row[8] for row in rows_perf],
#                            'seq_y': seq_y,
#                            'seq_scores': seq_scores}
#
#     '''1. Plotting perforation level Precision-Recall curve with the center overlapping metric'''
#     plt.figure()
#     plt.rc('font', size=15)
#     # plt.subplot(131)
#     for method, result in results.items():
#         plt.plot(result['perf_arcin'], result['perf_apcin'], label=method)
#
#     # plt.plot(0.68, 1, 's', label='Expert observer 2')
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('Recall')
#     plt.ylabel('Precision')
#     # plt.title('Precision-recall curve with the center overlap metric')
#     plt.legend(loc='lower left', fancybox=True, framealpha=0, fontsize='x-small')
#     plt.tight_layout()
#     plt.savefig(os.path.join(args.dir, 'sample_test_pr_curve_cin.png'), dpi=1200)
#     plt.show()
#     plt.close()
#
#     '''2. Plotting perforation level Precision-Recall curve with the IOU>=50% metric'''
#     plt.figure()
#     plt.rc('font', size=15)
#     # plt.subplot(132)
#     for method, result in results.items():
#         plt.plot(result['perf_ap50'], result['perf_ar50'], label=method)
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('Recall')
#     plt.ylabel('Precision')
#     # plt.title('Precision-recall curve with the IoU>=50 metric')
#     plt.legend(loc='lower left', fancybox=True, framealpha=0, fontsize='x-small')
#     plt.tight_layout()
#     plt.savefig(os.path.join(args.dir, 'sample_test_pr_curve_iou50.png'), dpi=1200)
#     plt.show()
#     plt.close()
#
#     '''3. Plotting acquisition level ROC curve with the center overlapping metric'''
#     plt.figure()
#     plt.rc('font', size=15)
#     # plt.subplot(133)
#     lw = 2
#     for method, result in results.items():
#         fpr, tpr, thresholds = roc_curve(result['seq_y'][0], result['seq_scores'], pos_label=1)
#         auc = roc_auc_score(result['seq_y'][0], result['seq_scores'])
#         plt.plot(fpr, tpr, lw=lw, label='{} (area = {:.3f})'.format(method, auc))  # color='darkorange',
#     # plt.plot(0, 0.71, 's', label='Expert observer 2')
#     plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
#     plt.xlim([-0.05, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('False Positive Rate')
#     plt.ylabel('True Positive Rate')
#     # plt.title('ROC curve for perforation classification on acquisition level')
#     plt.legend(loc='lower right', fancybox=True, framealpha=0, fontsize='x-small')
#     plt.tight_layout()
#     plt.savefig(os.path.join(args.dir, 'sample_test_series_ROC.png'), dpi=1200)
#     plt.show()
#     plt.close()
#
#
# def parse_args():
#     """
#     Argument parser for the main function
#     """
#     parser = argparse.ArgumentParser(description='Register a pair of images')
#     parser.add_argument('--dir', type=str, help='The dir path containing the result of runs of all models.')
#     parser.add_argument('--max_per_img', type=int, default=1, help='Max number of detections per image in test')
#
#     return parser.parse_args()
#
#
# if __name__ == '__main__':
#     """Train and cross validate various methods on the perforation data"""
#     random.seed(42)
#
#     args = parse_args()
#
#     logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
#                         format='%(asctime)s %(levelname)-8s %(message)s',
#                         handlers=[logging.StreamHandler(sys.stdout)])
#
#     collect_results(args)
#     print("Done!")