import argparse
import json
import logging
import os
import sys
from pathlib import Path
from tabulate import tabulate
from tools.sequence_optimization import optimize_sequence
from statistics import mean, stdev
import glob
logger = logging.getLogger(__name__)


def main(args):
    run_dirpaths = sorted(glob.glob(os.path.join(args.dir, 'run*/'), recursive=False))
    logger.info("Found {} runs of the experiment.".format(len(run_dirpaths)))

    methods = []
    for run_dirpath in run_dirpaths:
        methods.append([Path(p).name for p in glob.glob(os.path.join(run_dirpath, '*/'))])
    common_methods = set(methods[0])
    for m in methods[1:]:
        common_methods.intersection_update(m)
    logger.info("Found {} methods that exist in all {} runs".format(len(common_methods), len(run_dirpaths)))

    numfold_list = []
    for run_dirpath in run_dirpaths:
        for method in sorted(common_methods):
            fold_dirpaths = glob.glob(os.path.join(run_dirpath, method, 'fold[0-9]/'), recursive=False)
            numfold_list.append(len(fold_dirpaths))
    numfold = min(numfold_list)
    logger.info("Found at least {} folds in each method".format(numfold))
    # import re
    # m = re.search('Episode (\d+)', 'series[Episode 37]-03th_July_2010-YouTube', re.IGNORECASE)
    # m.group(1)

    folds = [*["fold{}".format(i) for i in range(numfold)], "all"]
    # folds = ["all"]
    for fold in folds:
        logger.info("Fold: {}".format(fold))
        performance_rows = []
        for method in sorted(common_methods):
            performance_dict_list = []
            optimized_performance_dict_list = []
            for run_dirpath in run_dirpaths:
                if fold == "all":
                    performance_path = glob.glob(os.path.join(run_dirpath, method, 'performance_max{}.json'.format(args.max_per_img)), recursive=False)[0]
                else:
                    performance_path = glob.glob(os.path.join(run_dirpath, method, fold, 'results_max{}'.format(args.max_per_img), 'performance_max{}.json'.format(args.max_per_img)), recursive=False)[0]
                logger.info("Collecting performance from {}".format(performance_path))
                performance_dict = json.load(open(performance_path, "r"))
                performance_dict_list.append(performance_dict)

                optimized_performance_dictpath = performance_path.replace("performance_max", "optimized_performance_max")
                if not os.path.isfile(optimized_performance_dictpath):
                    """Sequence optimization"""
                    raise ValueError("Missing sequence optimization results. Please check.")
                    # optimize_sequence(args)
                logger.info("Collecting performance from {}".format(optimized_performance_dictpath))
                optimized_performance_dict = json.load(open(optimized_performance_dictpath, "r"))
                optimized_performance_dict_list.append(optimized_performance_dict)
            performance_rows.append([method, False, mean([d['AP_cin'] for d in performance_dict_list]),
                                     mean([d['AR_cin'] for d in performance_dict_list]),
                                     mean([d['AUPRC'] for d in performance_dict_list]),
                                     stdev([d['AUPRC'] for d in performance_dict_list]),
                                     mean([d['AP50'] for d in performance_dict_list]),
                                     mean([d['AR50'] for d in performance_dict_list]),
                                     mean([d['Spec'] for d in performance_dict_list]),
                                     mean([d['Sens'] for d in performance_dict_list]),
                                     mean([d['Sens95'][0] for d in performance_dict_list]),
                                     stdev([d['Sens95'][0] for d in performance_dict_list]),
                                     mean([d['AUC'] for d in performance_dict_list]),
                                     stdev([d['AUC'] for d in performance_dict_list]),
                                     ["%.3f" % item['AUC'] for item in performance_dict_list],
                                     ["%.3f" % item['AUPRC'] for item in performance_dict_list],
                                     ['{:.3f}(Spec:{:.3f})'.format(item['Sens95'][0], item['Sens95'][1]) for item in
                                      performance_dict_list],
                                     mean([d['Spec95'][0] for d in performance_dict_list]),
                                     ['{:.3f}(Sens:{:.3f})'.format(item['Spec95'][0], item['Spec95'][1]) for item in
                                      performance_dict_list],
                                     ])
            performance_rows.append([method, True, mean([d['AP_cin'] for d in optimized_performance_dict_list]),
                                     mean([d['AR_cin'] for d in optimized_performance_dict_list]),
                                     mean([d['AUPRC'] for d in optimized_performance_dict_list]),
                                     stdev([d['AUPRC'] for d in optimized_performance_dict_list]),
                                     mean([d['AP50'] for d in optimized_performance_dict_list]),
                                     mean([d['AR50'] for d in optimized_performance_dict_list]),
                                     mean([d['Spec'] for d in optimized_performance_dict_list]),
                                     mean([d['Sens'] for d in optimized_performance_dict_list]),
                                     mean([d['Sens95'][0] for d in optimized_performance_dict_list]),
                                     stdev([d['Sens95'][0] for d in optimized_performance_dict_list]),
                                     mean([d['AUC'] for d in optimized_performance_dict_list]),
                                     stdev([d['AUC'] for d in optimized_performance_dict_list]),
                                     ["%.3f" % item['AUC'] for item in optimized_performance_dict_list],
                                     ["%.3f" % d['AUPRC'] for d in optimized_performance_dict_list],
                                     ['{:.3f}(Spec:{:.3f})'.format(item['Sens95'][0], item['Sens95'][1]) for item in
                                      optimized_performance_dict_list],
                                     mean([d['Spec95'][0] for d in optimized_performance_dict_list]),
                                     ['{:.3f}(Sens:{:.3f})'.format(item['Spec95'][0], item['Spec95'][1]) for item in
                                      optimized_performance_dict_list],
                                     ])

        table = tabulate(
            performance_rows,
            headers=['Method', 'Seq_opt', 'mAPcin', 'mARcin', 'AUPRC', 'AUPRC_std', 'mAP50', 'mAR50', 'mSpec', 'mSens',
                     'mSens95', 'mSens95_std', 'AUC_mean', 'AUC_std', 'AUCs', 'AUPRC', 'Sens95s', 'mSpec95', 'Spec95s'],
            tablefmt="pipe",
            floatfmt=("s", "r", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f",
                      "s", "s", "s", ".3f", "s"),
            stralign="center",
            numalign="center",
        )
        logger.info("Performance summary\n{}".format(table))


def parse_args():
    """
    Argument parser for the main function
    """
    parser = argparse.ArgumentParser(description='Calculate mean+std results of multiple runs')
    parser.add_argument('--dir', type=str, help='The folder path which contains all exp results')
    parser.add_argument('--max_per_img', type=int, default=1, help='Max number of detections per image in test')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    log_filepath = '{}/exp_summary_max{}.log'.format(args.dir, args.max_per_img)
    Path(log_filepath).parent.mkdir(parents=True, exist_ok=True)
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.FileHandler(log_filepath, mode='w'),
                                  logging.StreamHandler(sys.stdout)])

    main(args)
    print("Done!")
