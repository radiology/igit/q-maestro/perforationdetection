import argparse
import logging
import os
import random
import sys
from pathlib import Path
from statistics import median, mean, stdev

import mmcv
import numpy as np
from sklearn.metrics import roc_auc_score
from tabulate import tabulate

logger = logging.getLogger(__name__)


def ensemble_sequence_classification(result_dicts_list, loc_label, prob_thr=None):
    num_models = len(result_dicts_list)
    sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts_list[0]])
    # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in result_dicts_list[0]])
    gt_positive_sequences = [s for s in sequences if
                             any([d for d in result_dicts_list[0] if
                                  (s in d['file_name']) and (d['gt_loc'] is not None)])]
    gt_negative_sequences = [s for s in sequences if
                             not any([d for d in result_dicts_list[0] if
                                      (s in d['file_name']) and (d['gt_loc'] is not None)])]
    pd_positive_sequences = []
    for result_dicts in result_dicts_list:
        if prob_thr is None:
            pd_positive_sequences.extend([s for s in sequences if any([d for d in result_dicts if
                                                                       (s in d['file_name']) and (loc_label in d) and (
                                                                               d[loc_label] is not None)])])
        else:
            pd_positive_sequences.extend(
                [s for s in sequences if any([d for d in result_dicts if (s in d['file_name']) and
                                              (loc_label in d) and (d[loc_label] is not None) and (
                                                      d['scores'] >= prob_thr)])])
    pd_negative_sequences = [s for s in sequences if s not in pd_positive_sequences]
    logger.info("===========prob_thr = {} ===============".format(prob_thr))
    logger.info("Total number of predicted positive sequences from at least one model: {}".format(
        len(set(pd_positive_sequences))))
    logger.info("Total number of negative sequences: {}".format(len(pd_negative_sequences)))
    for num in range(num_models + 1):
        temp_positive = [s for s in set(pd_positive_sequences) if pd_positive_sequences.count(s) >= num]
        logger.info("Agreement num={}: num_seqs={}. seqs: {}".format(num, len(temp_positive), temp_positive))
    logger.info("========================================".format(prob_thr))
    pd_positive_sequences = [s for s in set(pd_positive_sequences) if
                             pd_positive_sequences.count(s) >= num_models / 2.0]
    pd_negative_sequences = [s for s in sequences if s not in pd_positive_sequences]

    tp, fn, tn, fp = 0, 0, 0, 0
    for sequence in sequences:
        if (sequence in gt_positive_sequences) and (sequence in pd_positive_sequences):
            tp += 1
            continue
        if (sequence in gt_positive_sequences) and (sequence in pd_negative_sequences):
            fn += 1
            continue
        if (sequence in gt_negative_sequences) and (sequence in pd_positive_sequences):
            fp += 1
            continue
        if (sequence in gt_negative_sequences) and (sequence in pd_negative_sequences):
            tn += 1
            continue

    assert len(
        sequences) == tp + tn + fp + fn, "Mismatch in number of sequences: TP+TN+FP+FN ={}, total sequences = {}".format(
        tp + tn + fp + fn, len(sequences))
    acc = (tp + tn) / (tp + tn + fp + fn)
    precision = tp / (tp + fp) if (tp + fp) else 0
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) else 0
    specificity = tn / (tn + fp)
    sensitivity = tp / (tp + fn)
    J = specificity + sensitivity - 1
    return acc, f1, precision, recall, specificity, sensitivity, J, tp, fp, tn, fn


def ensemble_learning(args, use_so_results, return_scores_only=False, return_preds_only=False):
    model_result_dicts_list = []
    for model in args.models:
        result_dirname = "{}_seqlen{}_cosinelr5e-04".format(model[0], model[1])
        result_filename = "{}_result_dicts_max{}.pkl".format(('overall', 'optimized')[use_so_results], args.max_per_img)
        model_result_filepath = os.path.join(args.run_dir, result_dirname, result_filename)
        model_result_dicts = mmcv.load(model_result_filepath)
        for i, d in enumerate(model_result_dicts):
            pos = int(np.argmax(model_result_dicts[i]['scores'], axis=0)) if model_result_dicts[i]['scores'] else None
            model_result_dicts[i]['scores'] = model_result_dicts[i]['scores'][pos] if pos is not None else None
            model_result_dicts[i]['pd_loc'] = model_result_dicts[i]['pd_loc'][pos] if pos is not None else None
        model_result_dicts_list.append(model_result_dicts)

    sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in model_result_dicts_list[0]])
    # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in model_result_dicts_list[0]])

    seq_y_true = []
    seq_y_score = []
    for s in sequences:
        frames = [d for d in model_result_dicts_list[0] if s in d['file_name']]
        seq_y_true.append(any([(f['gt_loc'] is not None) for f in frames]))
        seq_y_model_scores = []
        for model_result_dicts in model_result_dicts_list:
            frames = [d for d in model_result_dicts if s in d['file_name']]
            seq_y_model_scores.append(max([0] + [f['scores'] for f in frames if f['pd_loc'] is not None]))
        seq_y_score.append(median(seq_y_model_scores))
    seq_auc = roc_auc_score(seq_y_true, seq_y_score)
    if return_scores_only:
        return seq_y_true, seq_y_score

    sequence_dict_list = []
    rows_seq = []
    prob_score_thr_list = np.linspace(0.05, 0.95, 30)
    headers = ['Prob_thr', 'Acc', 'F1', 'AP', 'AR', 'Spec', 'Sens', 'J', 'TP', 'FP', 'TN', 'FN']
    for prob_score_thr in prob_score_thr_list:
        values = [prob_score_thr, *ensemble_sequence_classification(model_result_dicts_list, 'pd_loc', prob_score_thr)]
        rows_seq.append(values)
        sequence_dict_list.append(dict(zip(headers, values)))
    optimal_seq_row = max(sequence_dict_list, key=lambda x: x['J'])
    logger.info("Optimal ensemble performance: {}".format(optimal_seq_row))
    optimal_prob_thres = optimal_seq_row['Prob_thr']
    seq_y_pred = [s >= optimal_prob_thres for s in seq_y_score]
    if return_preds_only:
        return seq_y_true, seq_y_pred

    table = tabulate(
        rows_seq,
        headers=headers,
        tablefmt="pipe",
        floatfmt=(".2f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", "d", "d", "d", "d"),
        stralign="center",
        numalign="center",
    )
    logger.info(
        "==== Evaluation of ensemble sequence classification ====\n"
        "Num sequences: {}, AUC: {}\n{}".format(len(sequences), seq_auc, table))
    optimal_seq_row = max(sequence_dict_list, key=lambda x: x['J'])
    optimal_prob_thres = optimal_seq_row['Prob_thr']
    sens95_row = min(sequence_dict_list, key=lambda x: abs(x['Spec'] - 0.95))
    spec95_row = min(sequence_dict_list, key=lambda x: abs(x['Sens'] - 0.95))
    performance_dict = {'prob_thres': optimal_prob_thres,
                        'Spec': optimal_seq_row['Spec'],
                        'Sens': optimal_seq_row['Sens'],
                        'Sens95': (sens95_row['Sens'], sens95_row['Spec']),
                        'Spec95': (spec95_row['Spec'], spec95_row['Sens']),
                        'AUC': seq_auc}
    logger.info("Performance Summary: {}".format(performance_dict))

    return performance_dict


def parse_args():
    """
    Argument parser for the main function
    """
    parser = argparse.ArgumentParser(description='Model ensemble.')
    parser.add_argument('--dir', type=str, help='The input dir containing runs of cv results of various models.')
    parser.add_argument('--max_per_img', type=int, default=1, help='Max number of detections per image in test')

    return parser.parse_args()


if __name__ == '__main__':
    """Ensemble the bbox & probability results of various models based on majority voting."""
    random.seed(42)

    args = parse_args()

    models_to_ensemble = [
        ('retinanet_cosine12_bilstm', 5),
        ('retinanet_cosine12_bigru', 5),
        ('retinanet_cosine12_t11', 5),
        ('retinanet_cosine12_att', 5)]

    vars(args)['models'] = models_to_ensemble

    log_filepath = '{}/main_ensemble.log'.format(args.dir)
    Path(log_filepath).parent.mkdir(parents=True, exist_ok=True)
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.FileHandler(log_filepath, mode='w'),
                                  logging.StreamHandler(sys.stdout)])

    logger.info("Ensemble argument list: {}".format(sys.argv))
    performance_dicts = []
    for run in range(5):
        vars(args)['run_dir'] = os.path.join(args.dir, 'run{}'.format(run))
        performance_dicts.append(ensemble_learning(args, True))
    logger.info("================Ensemble learning performance summary===============")
    logger.info("mSpec: {:.3f}, mSens: {:.3f}, mSens95: {:.3f}, mSpec95: {:.3f}, mAUC: {:.3f}, stdAUC: {:.3f}, "
                "\nAUCs: {}, \nSens95s: {}, \nSpec95s: {}"
                "".format(mean([d['Spec'] for d in performance_dicts]),
                          mean([d['Sens'] for d in performance_dicts]),
                          mean([d['Sens95'][0] for d in performance_dicts]),
                          mean([d['Spec95'][0] for d in performance_dicts]),
                          mean([d['AUC'] for d in performance_dicts]),
                          stdev([d['AUC'] for d in performance_dicts]),
                          ["%.3f" % item['AUC'] for item in performance_dicts],
                          ['{:.3f}(Spec:{:.3f})'.format(item['Sens95'][0], item['Sens95'][1]) for item in
                           performance_dicts],
                          ['{:.3f}(Sens:{:.3f})'.format(item['Spec95'][0], item['Spec95'][1]) for item in
                           performance_dicts], ))
    print("Done!")
