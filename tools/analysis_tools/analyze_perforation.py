import argparse
import os
import mmcv
import numpy as np
from mmcv import Config, DictAction

from mmdet.datasets import build_dataset, get_loading_pipeline
import logging

import torch
import torchvision
from tabulate import tabulate

logger = logging.getLogger(__name__)

iou_thres_list = np.linspace(0, 1, 21)


def evaluate_center_in_iou(dataset, prediction_results):
    iou_cin_tp = 0  # predicted rect center locates in the ground truth rect.
    iou_cin_fp = 0
    iou_cin_fn = 0
    for i, (result,) in enumerate(zip(prediction_results)):
        # self.dataset[i] should not call directly because there is a risk of mismatch
        data_info = dataset.prepare_train_img(i)
        gt_loc_x1y1wh = data_info['ann_info']['bboxes'][0]  # There is maximum one perforation annotated per image.
        pd_loc_x1y1x2y2 = result[0].flatten()  # We only consider maximum one predicted bbox in one image
        # pd_loc_x1y1x2y2 = None if pd_loc_x1y1x2y2.size == 0 else pd_loc_x1y1x2y2[:-1]
        if (pd_loc_x1y1x2y2.size == 0) and (len(gt_loc_x1y1wh) != 0):
            iou_cin_fn += 1
            continue
        if cin(gt_loc_x1y1wh, pd_loc_x1y1x2y2):
            iou_cin_tp += 1
        else:
            iou_cin_fp += 1
            iou_cin_fn += 1
    _ap_cin = iou_cin_tp / (iou_cin_tp + iou_cin_fp)
    _ar_cin = iou_cin_tp / (iou_cin_tp + iou_cin_fn)
    return _ap_cin, _ar_cin


def cin(_gt_loc_x1y1wh, _pd_loc_x1y1x2y2):
    pd_loc_xc = (_pd_loc_x1y1x2y2[0] + _pd_loc_x1y1x2y2[2]) / 2
    pd_loc_yc = (_pd_loc_x1y1x2y2[1] + _pd_loc_x1y1x2y2[3]) / 2
    gt_minx = _gt_loc_x1y1wh[0]
    gt_maxx = _gt_loc_x1y1wh[0] + _gt_loc_x1y1wh[2]
    gt_miny = _gt_loc_x1y1wh[1]
    gt_maxy = _gt_loc_x1y1wh[1] + _gt_loc_x1y1wh[3]

    gt_loc_xc = _gt_loc_x1y1wh[0] + _gt_loc_x1y1wh[2] / 2
    gt_loc_yc = _gt_loc_x1y1wh[1] + _gt_loc_x1y1wh[3] / 2
    pd_minx = min(_pd_loc_x1y1x2y2[0], _pd_loc_x1y1x2y2[2])
    pd_maxx = max(_pd_loc_x1y1x2y2[0], _pd_loc_x1y1x2y2[2])
    pd_miny = min(_pd_loc_x1y1x2y2[1], _pd_loc_x1y1x2y2[3])
    pd_maxy = max(_pd_loc_x1y1x2y2[1], _pd_loc_x1y1x2y2[3])
    if (gt_maxx >= pd_loc_xc >= gt_minx) and (gt_maxy >= pd_loc_yc >= gt_miny):
        return True
    if (pd_maxx >= gt_loc_xc >= pd_minx) and (pd_maxy >= gt_loc_yc >= pd_miny):
        return True
    return False


def evaluate_iou(dataset, prediction_results):
    iou_tp_list = np.zeros(len(iou_thres_list))
    iou_fp_list = np.zeros(len(iou_thres_list))
    iou_fn_list = np.zeros(len(iou_thres_list))
    for i, iou_thres in enumerate(iou_thres_list):
        for r, (result,) in enumerate(zip(prediction_results)):
            # self.dataset[i] should not call directly because there is a risk of mismatch
            data_info = dataset.prepare_train_img(r)
            gt_loc_x1y1wh = torch.tensor(data_info['ann_info']['bboxes'][0])  # maximum 1 bbox annotated per image.
            pd_loc_x1y1x2y2 = result[0].flatten()  # We consider maximum 1 predicted bbox per image.
            if (pd_loc_x1y1x2y2.size == 0) and (len(gt_loc_x1y1wh) != 0):
                iou_fn_list[i] += 1
                continue
            pd_loc_x1y1x2y2 = torch.tensor(pd_loc_x1y1x2y2[:-1])
            gt_loc_x1y1x2y2 = torch.cat([gt_loc_x1y1wh[:2], gt_loc_x1y1wh[:2] + gt_loc_x1y1wh[2:]], 0)
            iou = torchvision.ops.box_iou(pd_loc_x1y1x2y2.unsqueeze(0),
                                          gt_loc_x1y1x2y2.unsqueeze(0)).squeeze().item()
            if (iou >= iou_thres) and (iou > 0):
                iou_tp_list[i] += 1
            else:
                iou_fp_list[i] += 1
                iou_fn_list[i] += 1
    _ap_list = np.divide(iou_tp_list, iou_tp_list + iou_fp_list)
    _ar_list = np.divide(iou_tp_list, iou_tp_list + iou_fn_list)
    return _ap_list, _ar_list


def evaluate_sequence_iou(dataset, prediction_results):
    _sequence_tp_list = np.zeros(len(iou_thres_list))
    _sequence_fp_list = np.zeros(len(iou_thres_list))
    _sequence_fn_list = np.zeros(len(iou_thres_list))
    sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in dataset.data_infos])
    for sequence in sequences:
        # sequence_results = [d for d in result_dicts if sequence in d['file_name']]
        sequence_ious = []
        for i, (result,) in enumerate(zip(prediction_results)):
            # self.dataset[i] should not call directly because there is a risk of mismatch
            data_info = dataset.prepare_train_img(i)
            if sequence != os.path.basename(os.path.dirname(data_info['filename'])):
                continue
            gt_loc_x1y1wh = torch.tensor(
                data_info['ann_info']['bboxes'][0])  # There is maximum one perforation annotated per image.
            pd_loc_x1y1x2y2 = result[0].flatten()  # We only consider maximum one predicted bbox in one image

            if (pd_loc_x1y1x2y2.size == 0) and (len(gt_loc_x1y1wh) != 0):
                sequence_ious.append(None)
                continue
            pd_loc_x1y1x2y2 = torch.tensor(pd_loc_x1y1x2y2[:-1])
            gt_loc_x1y1x2y2 = torch.cat([gt_loc_x1y1wh[:2], gt_loc_x1y1wh[:2] + gt_loc_x1y1wh[2:]], 0)
            iou = torchvision.ops.box_iou(pd_loc_x1y1x2y2.unsqueeze(0),
                                          gt_loc_x1y1x2y2.unsqueeze(0)).squeeze().item()
            sequence_ious.append(iou)
        for i, iou_thres in enumerate(iou_thres_list):
            if all(s is None for s in sequence_ious):
                _sequence_fn_list[i] += 1
                continue
            if any((s >= iou_thres) and (s > 0) for s in sequence_ious if s is not None):
                _sequence_tp_list[i] += 1
            else:
                _sequence_fp_list[i] += 1
                _sequence_fn_list[i] += 1
    _sequence_ap_list = np.divide(_sequence_tp_list, _sequence_tp_list + _sequence_fp_list)
    _sequence_ar_list = np.divide(_sequence_tp_list, _sequence_tp_list + _sequence_fn_list)
    return _sequence_ap_list, _sequence_ar_list, _sequence_tp_list, _sequence_fp_list, _sequence_fn_list


def evaluate_sequence_cin(dataset, prediction_results):
    _sequence_cin_tp = 0
    _sequence_cin_fp = 0
    _sequence_cin_fn = 0
    sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in dataset.data_infos])
    for sequence in sequences:
        # sequence_results = [d for d in result_dicts if sequence in d['file_name']]
        _sequence_cins = []
        for i, (result,) in enumerate(zip(prediction_results)):
            # self.dataset[i] should not call directly because there is a risk of mismatch
            data_info = dataset.prepare_train_img(i)
            if sequence != os.path.basename(os.path.dirname(data_info['filename'])):
                continue
            gt_loc_x1y1wh = data_info['ann_info']['bboxes'][
                0]  # There is maximum one perforation annotated per image.
            pd_loc_x1y1x2y2 = result[0].flatten()  # We only consider maximum one predicted bbox in one image

            if (pd_loc_x1y1x2y2.size == 0) and (len(gt_loc_x1y1wh) != 0):
                _sequence_cins.append(None)
                continue
            _sequence_cins.append(cin(gt_loc_x1y1wh, pd_loc_x1y1x2y2))
        if all(s is None for s in _sequence_cins):
            _sequence_cin_fn += 1
            continue
        if any(_sequence_cins):
            _sequence_cin_tp += 1
        else:
            _sequence_cin_fp += 1
            _sequence_cin_fn += 1

    _sequence_cin_ap = _sequence_cin_tp / (_sequence_cin_tp + _sequence_cin_fp)
    _sequence_cin_ar = _sequence_cin_tp / (_sequence_cin_tp + _sequence_cin_fn)
    return _sequence_cin_ap, _sequence_cin_ar, _sequence_cin_tp, _sequence_cin_fp, _sequence_cin_fn


def evaluate(result_dicts):
    def evaluate_center_in_iou():
        iou_cin_tp = 0  # predicted rect center locates in the ground truth rect.
        iou_cin_fp = 0
        iou_cin_fn = 0
        for result_dict in result_dicts:
            pd_loc_x1y1x2y2 = result_dict['pd_loc']
            gt_loc_x1y1wh = result_dict['gt_loc']
            if (pd_loc_x1y1x2y2 is None) and (gt_loc_x1y1wh is not None):
                iou_cin_fn += 1
                continue
            if cin(gt_loc_x1y1wh, pd_loc_x1y1x2y2):
                iou_cin_tp += 1
            else:
                iou_cin_fp += 1
                iou_cin_fn += 1
        _ap_cin = iou_cin_tp / (iou_cin_tp + iou_cin_fp)
        _ar_cin = iou_cin_tp / (iou_cin_tp + iou_cin_fn)
        return _ap_cin, _ar_cin

    def evaluate_iou():
        iou_tp_list = np.zeros(len(iou_thres_list))
        iou_fp_list = np.zeros(len(iou_thres_list))
        iou_fn_list = np.zeros(len(iou_thres_list))
        for i, iou_thres in enumerate(iou_thres_list):
            for result_dict in result_dicts:
                pd_loc_x1y1x2y2 = result_dict['pd_loc']
                gt_loc_x1y1wh = result_dict['gt_loc']
                if (pd_loc_x1y1x2y2 is None) and (gt_loc_x1y1wh is not None):
                    iou_fn_list[i] += 1
                    continue
                gt_loc_x1y1x2y2 = torch.cat([gt_loc_x1y1wh[:2], gt_loc_x1y1wh[:2] + gt_loc_x1y1wh[2:]], 0)
                iou = torchvision.ops.box_iou(pd_loc_x1y1x2y2.unsqueeze(0),
                                              gt_loc_x1y1x2y2.unsqueeze(0)).squeeze().item()
                if (iou >= iou_thres) and (iou > 0):
                    iou_tp_list[i] += 1
                else:
                    iou_fp_list[i] += 1
                    iou_fn_list[i] += 1
        _ap_list = np.divide(iou_tp_list, iou_tp_list + iou_fp_list)
        _ar_list = np.divide(iou_tp_list, iou_tp_list + iou_fn_list)
        return _ap_list, _ar_list

    def evaluate_sequence_iou():
        _sequence_tp_list = np.zeros(len(iou_thres_list))
        _sequence_fp_list = np.zeros(len(iou_thres_list))
        _sequence_fn_list = np.zeros(len(iou_thres_list))
        sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts])
        for sequence in sequences:
            sequence_results = [d for d in result_dicts if sequence in d['file_name']]
            sequence_ious = []
            for result_dict in sequence_results:
                pd_loc_x1y1x2y2 = result_dict['pd_loc']
                gt_loc_x1y1wh = result_dict['gt_loc']
                if (pd_loc_x1y1x2y2 is None) and (gt_loc_x1y1wh is not None):
                    sequence_ious.append(None)
                    continue
                gt_loc_x1y1x2y2 = torch.cat([gt_loc_x1y1wh[:2], gt_loc_x1y1wh[:2] + gt_loc_x1y1wh[2:]], 0)
                iou = torchvision.ops.box_iou(pd_loc_x1y1x2y2.unsqueeze(0),
                                              gt_loc_x1y1x2y2.unsqueeze(0)).squeeze().item()
                sequence_ious.append(iou)
            for i, iou_thres in enumerate(iou_thres_list):
                if all(s is None for s in sequence_ious):
                    _sequence_fn_list[i] += 1
                    continue
                if any((s >= iou_thres) and (s > 0) for s in sequence_ious if s is not None):
                    _sequence_tp_list[i] += 1
                else:
                    _sequence_fp_list[i] += 1
                    _sequence_fn_list[i] += 1
        _sequence_ap_list = np.divide(_sequence_tp_list, _sequence_tp_list + _sequence_fp_list)
        _sequence_ar_list = np.divide(_sequence_tp_list, _sequence_tp_list + _sequence_fn_list)
        return _sequence_ap_list, _sequence_ar_list, _sequence_tp_list, _sequence_fp_list, _sequence_fn_list

    def cin(_gt_loc_x1y1wh, _pd_loc_x1y1x2y2):
        pd_loc_xc = (_pd_loc_x1y1x2y2[0] + _pd_loc_x1y1x2y2[2]) / 2
        pd_loc_yc = (_pd_loc_x1y1x2y2[1] + _pd_loc_x1y1x2y2[3]) / 2
        gt_minx = _gt_loc_x1y1wh[0]
        gt_maxx = _gt_loc_x1y1wh[0] + _gt_loc_x1y1wh[2]
        gt_miny = _gt_loc_x1y1wh[1]
        gt_maxy = _gt_loc_x1y1wh[1] + _gt_loc_x1y1wh[3]

        gt_loc_xc = _gt_loc_x1y1wh[0] + _gt_loc_x1y1wh[2] / 2
        gt_loc_yc = _gt_loc_x1y1wh[1] + _gt_loc_x1y1wh[3] / 2
        pd_minx = min(_pd_loc_x1y1x2y2[0], _pd_loc_x1y1x2y2[2])
        pd_maxx = max(_pd_loc_x1y1x2y2[0], _pd_loc_x1y1x2y2[2])
        pd_miny = min(_pd_loc_x1y1x2y2[1], _pd_loc_x1y1x2y2[3])
        pd_maxy = max(_pd_loc_x1y1x2y2[1], _pd_loc_x1y1x2y2[3])
        if (gt_maxx >= pd_loc_xc >= gt_minx) and (gt_maxy >= pd_loc_yc >= gt_miny):
            return True
        if (pd_maxx >= gt_loc_xc >= pd_minx) and (pd_maxy >= gt_loc_yc >= pd_miny):
            return True
        return False

    def evaluate_sequence_cin():
        _sequence_cin_tp = 0
        _sequence_cin_fp = 0
        _sequence_cin_fn = 0
        sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts])
        for sequence in sequences:
            frame_results = [d for d in result_dicts if sequence in d['file_name']]
            _sequence_cins = []
            for frame_result in frame_results:
                pd_loc_x1y1x2y2 = frame_result['pd_loc']
                gt_loc_x1y1wh = frame_result['gt_loc']
                if (pd_loc_x1y1x2y2 is None) and (gt_loc_x1y1wh is not None):
                    _sequence_cins.append(None)
                    continue
                _sequence_cins.append(cin(gt_loc_x1y1wh, pd_loc_x1y1x2y2))
            if all(s is None for s in _sequence_cins):
                _sequence_cin_fn += 1
                continue
            if any(_sequence_cins):
                _sequence_cin_tp += 1
            else:
                _sequence_cin_fp += 1
                _sequence_cin_fn += 1

        _sequence_cin_ap = _sequence_cin_tp / (_sequence_cin_tp + _sequence_cin_fp)
        _sequence_cin_ar = _sequence_cin_tp / (_sequence_cin_tp + _sequence_cin_fn)
        return _sequence_cin_ap, _sequence_cin_ar, _sequence_cin_tp, _sequence_cin_fp, _sequence_cin_fn

    ap_cin, ar_cin = evaluate_center_in_iou()
    iou_thres_list = np.linspace(0, 1, 21)
    ap_list, ar_list = evaluate_iou()
    sequence_ap_list, sequence_ar_list, sequence_tp_list, sequence_fp_list, sequence_fn_list = evaluate_sequence_iou()
    sequence_cin_ap, sequence_cin_ar, sequence_cin_tp, sequence_cin_fp, sequence_cin_fn = evaluate_sequence_cin()

    table = tabulate(
        [['Perf-AP', ap_cin, *ap_list], ['Perf-AR', ar_cin, *ar_list], ['Seq-AP', sequence_cin_ap, *sequence_ap_list],
         ['Seq-AR', sequence_cin_ar, *sequence_ar_list], ['Seq-TP', sequence_cin_tp, *sequence_tp_list],
         ['Seq-FP', sequence_cin_fp, *sequence_fp_list], ['Seq-FN', sequence_cin_fn, *sequence_fn_list]],
        headers=['IOU', 'center_in', *['{:.2f}'.format(t) for t in iou_thres_list]],
        tablefmt="pipe",
        floatfmt=".3f",
        stralign="center",
        numalign="center",
    )

    logger.info(
        "Evaluation results -- Num images: {}\n{}".format(len(result_dicts), table))
    return ap_list, ar_list, ap_cin, ar_cin


def evaluate_perforation(config_filepath, dataset_json_filepath, prediction_results_dict):
    """Evaluate perforation.

    Args:
        prediction_results_dict:
        dataset_json_filepath:
        config_filepath:
    """
    cfg = Config.fromfile(config_filepath)
    cfg.data.test.ann_file = dataset_json_filepath
    cfg.data.test.test_mode = True
    if cfg.get('custom_imports', None):
        from mmcv.utils import import_modules_from_strings
        import_modules_from_strings(**cfg['custom_imports'])

    cfg.data.test.pop('samples_per_gpu', 0)
    cfg.data.test.pipeline = get_loading_pipeline(cfg.data.train.pipeline)
    dataset = build_dataset(cfg.data.test)

    eval_dicts = []
    for i, result in enumerate(prediction_results_dict):
        # self.dataset[i] should not call directly because there is a risk of mismatch
        data_info = dataset.prepare_train_img(i)

        gt_loc_x1y1wh = torch.tensor(data_info['ann_info']['bboxes'][0])
        gt_loc_x1y1wh = gt_loc_x1y1wh if len(gt_loc_x1y1wh) != 0 else None

        pd_loc_x1y1x2y2 = result[0].flatten()
        pd_score = pd_loc_x1y1x2y2[-1] if pd_loc_x1y1x2y2.size != 0 else None
        pd_loc_x1y1x2y2 = torch.tensor(pd_loc_x1y1x2y2[:-1]) if pd_loc_x1y1x2y2.size != 0 else None
        eval_dicts.append({'file_name': data_info['filename'], 'gt_loc': gt_loc_x1y1wh,
                           'pd_loc': pd_loc_x1y1x2y2, 'scores': pd_score})
    evaluate(eval_dicts)
