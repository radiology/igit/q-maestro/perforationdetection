import argparse
import glob
import logging
import os
import random
import shutil
import sys
from pathlib import Path
import mmcv

logger = logging.getLogger(__name__)


def generate_downsampled_jsons(in_json_dir, out_json_dir, downsample_ratio=0.8):
    Path(out_json_dir).mkdir(parents=True, exist_ok=True)
    in_json_filepaths = glob.glob(os.path.join(in_json_dir, '*.json'), recursive=False)
    for in_json_filepath in in_json_filepaths:
        out_json_filepath = os.path.join(out_json_dir, os.path.basename(in_json_filepath))
        if 'train' not in in_json_filepath:
            shutil.copyfile(in_json_filepath, out_json_filepath)
        else:
            train_dicts = mmcv.load(in_json_filepath)
            patient_ids = set([os.path.basename(Path(i['file_name']).parent.parent) for i in train_dicts['images']])

            sampled_patient_ids = random.sample(patient_ids, int(len(patient_ids)*downsample_ratio))
            sampled_image_ids = [i['id'] for i in train_dicts['images'] if
                                 os.path.basename(Path(i['file_name']).parent.parent) in sampled_patient_ids]
            train_dicts['images'] = [i for i in train_dicts['images'] if i['id'] in sampled_image_ids]
            train_dicts['annotations'] = [a for a in train_dicts['annotations'] if a['image_id'] in sampled_image_ids]
            mmcv.dump(train_dicts, out_json_filepath)


def parse_args():
    """
    Argument parser for the main function
    """
    parser = argparse.ArgumentParser(description='Register a pair of images')
    parser.add_argument('--in_json_dir', type=str, help='Input dir which contains json files to down sample')

    return parser.parse_args()


if __name__ == '__main__':
    """Train and cross validate various methods on the perforation data"""
    random.seed(42)
    args = parse_args()

    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.StreamHandler(sys.stdout)])

    logger.info("Argument list: {}".format(sys.argv))

    in_json_dir = args.in_json_dir
    out_json_dir = os.path.join(args.in_json_dir, 'sampled', '8t')
    generate_downsampled_jsons(in_json_dir, out_json_dir, downsample_ratio=8/9.0)

    for num in range(7, 0, -1):
        in_json_dir = out_json_dir
        out_json_dir = os.path.join(args.in_json_dir, 'sampled', '{}t'.format(num))
        generate_downsampled_jsons(in_json_dir, out_json_dir, downsample_ratio=num/float(num+1))
    print("Done!")
