import os
from pathlib import Path
import logging
import cv2
import mmcv
import numpy as np
import torch
import torchvision
from natsort import natsorted
from sklearn.metrics import roc_auc_score, auc
from tabulate import tabulate

logger = logging.getLogger(__name__)


def cin_x1y1wh(a, b):
    a_xc = a[0] + a[2] / 2
    a_yc = a[1] + a[3] / 2
    a_minx = a[0]
    a_maxx = a[0] + a[2]
    a_miny = a[1]
    a_maxy = a[1] + a[3]

    b_xc = b[0] + b[2] / 2
    b_yc = b[1] + b[3] / 2
    b_minx = b[0]
    b_maxx = b[0] + b[2]
    b_miny = b[1]
    b_maxy = b[1] + b[3]

    if (a_maxx >= b_xc >= a_minx) and (a_maxy >= b_yc >= a_miny):
        return True
    if (b_maxx >= a_xc >= b_minx) and (b_maxy >= a_yc >= b_miny):
        return True
    return False


def cin(_gt_loc_x1y1wh, _pd_loc_x1y1x2y2):
    pd_loc_xc = (_pd_loc_x1y1x2y2[0] + _pd_loc_x1y1x2y2[2]) / 2
    pd_loc_yc = (_pd_loc_x1y1x2y2[1] + _pd_loc_x1y1x2y2[3]) / 2
    gt_minx = _gt_loc_x1y1wh[0]
    gt_maxx = _gt_loc_x1y1wh[0] + _gt_loc_x1y1wh[2]
    gt_miny = _gt_loc_x1y1wh[1]
    gt_maxy = _gt_loc_x1y1wh[1] + _gt_loc_x1y1wh[3]

    gt_loc_xc = _gt_loc_x1y1wh[0] + _gt_loc_x1y1wh[2] / 2
    gt_loc_yc = _gt_loc_x1y1wh[1] + _gt_loc_x1y1wh[3] / 2
    pd_minx = min(_pd_loc_x1y1x2y2[0], _pd_loc_x1y1x2y2[2])
    pd_maxx = max(_pd_loc_x1y1x2y2[0], _pd_loc_x1y1x2y2[2])
    pd_miny = min(_pd_loc_x1y1x2y2[1], _pd_loc_x1y1x2y2[3])
    pd_maxy = max(_pd_loc_x1y1x2y2[1], _pd_loc_x1y1x2y2[3])
    if (gt_maxx >= pd_loc_xc >= gt_minx) and (gt_maxy >= pd_loc_yc >= gt_miny):
        return True
    if (pd_maxx >= gt_loc_xc >= pd_minx) and (pd_maxy >= gt_loc_yc >= pd_miny):
        return True
    return False


def cal_iou_x1y1wh(a_x1y1wh, b_x1y1wh):
    a_x1y1x2y2 = torch.cat([a_x1y1wh[:2], a_x1y1wh[:2] + a_x1y1wh[2:]], 0)
    b_x1y1x2y2 = torch.cat([b_x1y1wh[:2], b_x1y1wh[:2] + b_x1y1wh[2:]], 0)
    return torchvision.ops.box_iou(b_x1y1x2y2.unsqueeze(0), a_x1y1x2y2.unsqueeze(0)).squeeze().item()


def cal_iou(gt_loc_x1y1wh, pd_loc_x1y1x2y2):
    gt_loc_x1y1x2y2 = torch.cat([gt_loc_x1y1wh[:2], gt_loc_x1y1wh[:2] + gt_loc_x1y1wh[2:]], 0)
    return torchvision.ops.box_iou(pd_loc_x1y1x2y2.unsqueeze(0), gt_loc_x1y1x2y2.unsqueeze(0)).squeeze().item()


def generate_subset_json(full_set, subset, out_jsonpath):
    subset = natsorted(subset)
    images = []
    annotations = []
    interob_annotations = []
    areas = []
    for idx, image_filepath in enumerate(full_set):
        if image_filepath not in subset:
            continue

        img_height, img_width = cv2.imread(image_filepath, cv2.IMREAD_GRAYSCALE).shape
        images.append(dict(id=idx, file_name=image_filepath, height=img_height, width=img_width))

        label_filepath = image_filepath.replace('images', 'labels').replace('.jpg', '.txt')
        if os.path.isfile(label_filepath):
            with open(label_filepath) as f:
                lines = f.read().splitlines()
            x_center, y_center, object_width, object_height = [float(n) for n in lines[0].split()[1:]]
            x_min = x_center - object_width / 2
            y_min = y_center - object_height / 2
            x_min, y_min = int(x_min * img_width), int(y_min * img_height)
            object_width, object_height = int(object_width * img_width), int(object_height * img_height)

            data_anno = dict(image_id=idx, id=idx, category_id=0, bbox=[x_min, y_min, object_width, object_height],
                             interobserver_bbox=[], area=object_width * object_height, iscrowd=0)
            areas.append(object_width * object_height)
            annotations.append(data_anno)

        interobserver_label_filepath = image_filepath.replace('images', 'second_ob_labels').replace('.jpg', '.txt')
        if os.path.isfile(interobserver_label_filepath):
            with open(interobserver_label_filepath) as f:
                lines = f.read().splitlines()
            x_center, y_center, object_width, object_height = [float(n) for n in lines[0].split()[1:]]
            x_min = x_center - object_width / 2
            y_min = y_center - object_height / 2
            x_min, y_min = int(x_min * img_width), int(y_min * img_height)
            object_width, object_height = int(object_width * img_width), int(object_height * img_height)
            interob_data_anno = dict(image_id=idx, id=idx, category_id=0,
                                     bbox=[x_min, y_min, object_width, object_height],
                                     area=object_width * object_height, iscrowd=0)
            interob_annotations.append(interob_data_anno)
    logger.info("Length of images: {} and labels: {}".format(len(images), len(annotations)))
    coco_format_dict = dict(
        images=images,
        annotations=annotations,
        interob_annotations=interob_annotations,
        categories=[{'id': 0, 'name': 'perforation'}])
    patients_ids = set([os.path.basename(Path(f).parent.parent) for f in subset])
    sequence_ids = set([os.path.basename(os.path.dirname(f)) for f in subset])
    image_ids = [c_dict["id"] for c_dict in coco_format_dict['images']]
    logger.info("Saved json with {} patients, {} sequences, {} images to json: {}"
                "".format(len(patients_ids), len(sequence_ids), len(subset), out_jsonpath))
    logger.info("Patient ids: {}".format(patients_ids))
    logger.info("Sequence ids: {}".format(sequence_ids))
    logger.info("Image ids: {}".format(image_ids))
    if areas:
        logger.info("Area statistics: max: {}, min:{}, avg:{}, std: {}".format(
            max(areas), min(areas), np.mean(np.array(areas)), np.std(np.array(areas))))
    mmcv.dump(coco_format_dict, out_jsonpath)


def evaluate_image_classification(result_dicts, prob_score_thr):
    tp = len([d for d in result_dicts if
              (d['gt_loc'] is not None) and (d['pd_loc'] is not None) and (d['scores'] >= prob_score_thr)])
    fn = len([d for d in result_dicts if
              (d['gt_loc'] is not None) and ((d['pd_loc'] is None) or (d['scores'] < prob_score_thr))])
    tn = len([d for d in result_dicts if
              (d['gt_loc'] is None) and ((d['pd_loc'] is None) or (d['scores'] < prob_score_thr))])
    fp = len([d for d in result_dicts if
              (d['gt_loc'] is None) and (d['pd_loc'] is not None) and (d['scores'] >= prob_score_thr)])
    assert len(
        result_dicts) == tp + tn + fp + fn, "Mismatch in number of images: TP+TN+FP+FN ={}, total results = {}".format(
        tp + tn + fp + fn, len(result_dicts))
    acc = (tp + tn) / (tp + tn + fp + fn)
    precision = tp / (tp + fp) if (tp + fp) else 0
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) else 0
    specificity = tn / (tn + fp)
    sensitivity = tp / (tp + fn)
    return [acc, f1, precision, recall, specificity, sensitivity, tp, fp, tn, fn]


def evaluate_sequence_classification(result_dicts, loc_label, prob_thr=None):
    sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts])
    # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in result_dicts])
    gt_positive_sequences = [s for s in sequences if
                             any([d for d in result_dicts if (s in d['file_name']) and (d['gt_loc'] is not None)])]
    gt_negative_sequences = [s for s in sequences if
                             not any([d for d in result_dicts if (s in d['file_name']) and (d['gt_loc'] is not None)])]
    if prob_thr is None:
        pd_positive_sequences = [s for s in sequences if any([d for d in result_dicts if
                                                              (s in d['file_name']) and (loc_label in d) and (
                                                                      d[loc_label] is not None)])]
    else:
        pd_positive_sequences = [s for s in sequences if any([d for d in result_dicts if (s in d['file_name']) and
                                                              (loc_label in d) and (d[loc_label] is not None) and (
                                                                      d['scores'] >= prob_thr)])]
    pd_negative_sequences = [s for s in sequences if s not in pd_positive_sequences]

    tp, fn, tn, fp = 0, 0, 0, 0
    for sequence in sequences:
        if (sequence in gt_positive_sequences) and (sequence in pd_positive_sequences):
            tp += 1
            continue
        if (sequence in gt_positive_sequences) and (sequence in pd_negative_sequences):
            fn += 1
            continue
        if (sequence in gt_negative_sequences) and (sequence in pd_positive_sequences):
            fp += 1
            continue
        if (sequence in gt_negative_sequences) and (sequence in pd_negative_sequences):
            tn += 1
            continue

    assert len(
        sequences) == tp + tn + fp + fn, "Mismatch in number of sequences: TP+TN+FP+FN ={}, total sequences = {}".format(
        tp + tn + fp + fn, len(sequences))
    acc = (tp + tn) / (tp + tn + fp + fn)
    precision = tp / (tp + fp) if (tp + fp) else 0
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) else 0
    specificity = tn / (tn + fp)
    sensitivity = tp / (tp + fn)
    J = specificity + sensitivity - 1
    return acc, f1, precision, recall, specificity, sensitivity, J, tp, fp, tn, fn


def evaluate_perf_localization_cin(result_dicts, loc_label, prob_thr=None):
    for i, d in enumerate(result_dicts):
        result_dicts[i]['gt_loc'] = result_dicts[i]['gt_loc'] if ('gt_loc' in result_dicts[i]) and (
                result_dicts[i]['gt_loc'] is not None) else None
        result_dicts[i][loc_label] = result_dicts[i][loc_label] if (loc_label in result_dicts[i]) and (
                result_dicts[i][loc_label] is not None) else None

    perforations = [d for d in result_dicts if (d['gt_loc'] is not None)]
    if prob_thr is None:
        tp = len([d for d in perforations if (d[loc_label] is not None) and cin_x1y1wh(d['gt_loc'], d[loc_label])])
        fn = len([d for d in perforations if ((d[loc_label] is None) or (not cin_x1y1wh(d['gt_loc'], d[loc_label])))])
        # tn = len([d for d in result_dicts if (d['gt_loc'] is None) and ((d[loc_label] is None) or (d['scores'] < prob_thr))])
        fp = len([d for d in result_dicts if (d['gt_loc'] is None) and (d[loc_label] is not None)])
        fp += len([d for d in result_dicts if (d['gt_loc'] is not None) and (d[loc_label] is not None) and
                   (not cin_x1y1wh(d['gt_loc'], d[loc_label]))])
    else:
        tp = len([d for d in perforations if
                  (d[loc_label] is not None) and (d['scores'] >= prob_thr) and cin(d['gt_loc'], d[loc_label])])
        fn = len([d for d in perforations if
                  ((d[loc_label] is None) or (d['scores'] < prob_thr) or (not cin(d['gt_loc'], d[loc_label])))])
        # tn = len([d for d in result_dicts if (d['gt_loc'] is None) and ((d[loc_label] is None) or (d['scores'] < prob_thr))])
        fp = len([d for d in result_dicts if
                  (d['gt_loc'] is None) and (d[loc_label] is not None) and (d['scores'] >= prob_thr)])
        fp += len([d for d in result_dicts if (d['gt_loc'] is not None) and (d[loc_label] is not None) and
                   (d['scores'] >= prob_thr) and (not cin(d['gt_loc'], d[loc_label]))])

    assert len(
        perforations) == tp + fn, "Mismatch in number of perforations: TP+FN ={}, total perforations = {}".format(
        tp + fn, len(perforations))
    precision = tp / (tp + fp) if (tp + fp) else 0
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) else 0
    return precision, recall, f1, tp, fp, fn


def evaluate_perf_localization_iou(result_dicts, loc_label, prob_thr=None):
    for i, d in enumerate(result_dicts):
        result_dicts[i]['gt_loc'] = d['gt_loc'] if ('gt_loc' in d) and (d['gt_loc'] is not None) else None
        if (loc_label not in d) or (d[loc_label] is None):
            result_dicts[i][loc_label] = None
            continue
        if loc_label == 'interob_loc':
            result_dicts[i][loc_label] = torch.cat([d[loc_label][:2], d[loc_label][:2] + d[loc_label][2:]], 0)

    perfs = [d for d in result_dicts if (d['gt_loc'] is not None)]
    if prob_thr is None:
        tp = len([d for d in perfs if (d[loc_label] is not None) and (cal_iou(d['gt_loc'], d[loc_label]) >= 0.5)])
        fn = len([d for d in perfs if ((d[loc_label] is None) or (cal_iou(d['gt_loc'], d[loc_label]) < 0.5))])
        # tn = len([d for d in result_dicts if (d['gt_loc'] is None) and ((d['pd_loc'] is None) or (d['scores'] < prob_score_thr))])
        fp = len([d for d in result_dicts if (d['gt_loc'] is None) and (d[loc_label] is not None)])
        fp += len([d for d in result_dicts if (d['gt_loc'] is not None) and (d[loc_label] is not None) and
                   (cal_iou(d['gt_loc'], d[loc_label]) < 0.5)])
    else:
        tp = len([d for d in perfs if (d[loc_label] is not None) and (d['scores'] >= prob_thr) and (
                cal_iou(d['gt_loc'], d[loc_label]) >= 0.5)])
        fn = len([d for d in perfs if
                  ((d[loc_label] is None) or (d['scores'] < prob_thr) or (cal_iou(d['gt_loc'], d[loc_label]) < 0.5))])
        fp = len([d for d in result_dicts if
                  (d['gt_loc'] is None) and (d[loc_label] is not None) and (d['scores'] >= prob_thr)])
        fp += len([d for d in result_dicts if (d['gt_loc'] is not None) and (d[loc_label] is not None) and
                   (d['scores'] >= prob_thr) and (cal_iou(d['gt_loc'], d[loc_label]) < 0.5)])

    assert len(
        perfs) == tp + fn, "Mismatch in number of perforations: TP+FN ={}, total perforations = {}".format(
        tp + fn, len(perfs))
    precision = tp / (tp + fp) if (tp + fp) else 0
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) else 0
    return precision, recall, f1, tp, fp, fn


def evaluate(result_dicts):
    def evaluate_center_in_iou():
        iou_cin_tp = 0  # predicted rect center locates in the ground truth rect.
        iou_cin_fp = 0
        iou_cin_fn = 0
        iou_cin_tn = 0
        for result_dict in result_dicts:
            pd_loc_x1y1x2y2 = result_dict['pd_loc']
            gt_loc_x1y1wh = result_dict['gt_loc']
            if gt_loc_x1y1wh is None:
                if pd_loc_x1y1x2y2 is not None:
                    iou_cin_fp += 1
                else:
                    iou_cin_tn += 1
                continue
            elif pd_loc_x1y1x2y2 is None:
                iou_cin_fn += 1
                continue
            if cin(gt_loc_x1y1wh, pd_loc_x1y1x2y2):
                iou_cin_tp += 1
            else:
                iou_cin_fp += 1
                iou_cin_fn += 1

        _ap_cin, _ar_cin = 0, 0
        if iou_cin_tp + iou_cin_fp != 0:
            _ap_cin = iou_cin_tp / (iou_cin_tp + iou_cin_fp)
        else:
            logger.warning("encountered division by zero when calculating _ap_cin. Set result to zero.")
        if iou_cin_tp + iou_cin_fn != 0:
            _ar_cin = iou_cin_tp / (iou_cin_tp + iou_cin_fn)
        else:
            logger.warning("encountered division by zero when calculating _ar_cin. Set result to zero.")
        return _ap_cin, _ar_cin

    def evaluate_iou():
        iou_tp_list = np.zeros(len(iou_thres_list))
        iou_fp_list = np.zeros(len(iou_thres_list))
        iou_fn_list = np.zeros(len(iou_thres_list))
        iou_tn_list = np.zeros(len(iou_thres_list))
        for i, iou_thres in enumerate(iou_thres_list):
            for result_dict in result_dicts:
                pd_loc_x1y1x2y2 = result_dict['pd_loc']
                gt_loc_x1y1wh = result_dict['gt_loc']
                if gt_loc_x1y1wh is None:
                    if pd_loc_x1y1x2y2 is not None:
                        iou_fp_list[i] += 1
                    else:
                        iou_tn_list[i] += 1
                    continue
                elif pd_loc_x1y1x2y2 is None:
                    iou_fn_list[i] += 1
                    continue
                gt_loc_x1y1x2y2 = torch.cat([gt_loc_x1y1wh[:2], gt_loc_x1y1wh[:2] + gt_loc_x1y1wh[2:]], 0)
                iou = torchvision.ops.box_iou(pd_loc_x1y1x2y2.unsqueeze(0),
                                              gt_loc_x1y1x2y2.unsqueeze(0)).squeeze().item()
                if (iou >= iou_thres) and (iou > 0):
                    iou_tp_list[i] += 1
                else:
                    iou_fp_list[i] += 1
                    iou_fn_list[i] += 1

        _ap_list = [((tp / pp) if int(pp != 0) else 0) for (tp, pp) in zip(iou_tp_list, iou_tp_list + iou_fp_list)]
        _ar_list = [((tp / pp) if int(pp != 0) else 0) for (tp, pp) in zip(iou_tp_list, iou_tp_list + iou_fn_list)]
        return _ap_list, _ar_list

    def evaluate_sequence_iou():
        _sequence_tp_list = np.zeros(len(iou_thres_list), dtype=int)
        _sequence_fp_list = np.zeros(len(iou_thres_list), dtype=int)
        _sequence_fn_list = np.zeros(len(iou_thres_list), dtype=int)
        _sequence_tn_list = np.zeros(len(iou_thres_list), dtype=int)
        sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts])
        # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in result_dicts])
        for sequence in sequences:
            sequence_results = [d for d in result_dicts if sequence in d['file_name']]
            sequence_ious = []
            for result_dict in sequence_results:
                pd_loc_x1y1x2y2 = result_dict['pd_loc']
                gt_loc_x1y1wh = result_dict['gt_loc']
                if (pd_loc_x1y1x2y2 is None) or (gt_loc_x1y1wh is None):
                    sequence_ious.append({'gt': gt_loc_x1y1wh, 'pd': pd_loc_x1y1x2y2, 'iou': None})
                    continue
                gt_loc_x1y1x2y2 = torch.cat([gt_loc_x1y1wh[:2], gt_loc_x1y1wh[:2] + gt_loc_x1y1wh[2:]], 0)
                iou = torchvision.ops.box_iou(pd_loc_x1y1x2y2.unsqueeze(0),
                                              gt_loc_x1y1x2y2.unsqueeze(0)).squeeze().item()
                sequence_ious.append({'gt': gt_loc_x1y1wh, 'pd': pd_loc_x1y1x2y2, 'iou': iou})
            for i, iou_thres in enumerate(iou_thres_list):
                if all(s['gt'] is None for s in sequence_ious):
                    if any(s['pd'] is not None for s in sequence_ious):
                        _sequence_fp_list[i] += 1
                    else:
                        _sequence_tn_list[i] += 1
                else:
                    if any((s['iou'] >= iou_thres) and (s['iou'] > 0) for s in sequence_ious if s['iou'] is not None):
                        _sequence_tp_list[i] += 1
                    else:
                        _sequence_fn_list[i] += 1
        _sequence_ap_list = [((tp / pp) if int(pp != 0) else 0) for (tp, pp) in
                             zip(_sequence_tp_list, _sequence_tp_list + _sequence_fp_list)]
        _sequence_ar_list = [((tp / pp) if int(pp != 0) else 0) for (tp, pp) in
                             zip(_sequence_tp_list, _sequence_tp_list + _sequence_fn_list)]
        return _sequence_ap_list, _sequence_ar_list, _sequence_tp_list, _sequence_fp_list, _sequence_fn_list, _sequence_tn_list

    def evaluate_sequence_cin():
        _sequence_cin_tp = 0
        _sequence_cin_fp = 0
        _sequence_cin_fn = 0
        _sequence_cin_tn = 0
        sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts])
        # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in result_dicts])
        for sequence in sequences:
            frame_results = [d for d in result_dicts if sequence in d['file_name']]
            _sequence_cins = []
            for frame_result in frame_results:
                pd_loc_x1y1x2y2 = frame_result['pd_loc']
                gt_loc_x1y1wh = frame_result['gt_loc']
                if (pd_loc_x1y1x2y2 is None) or (gt_loc_x1y1wh is None):
                    _sequence_cins.append({'gt': gt_loc_x1y1wh, 'pd': pd_loc_x1y1x2y2, 'cin': None})
                    continue
                _sequence_cins.append(
                    {'gt': gt_loc_x1y1wh, 'pd': pd_loc_x1y1x2y2, 'cin': cin(gt_loc_x1y1wh, pd_loc_x1y1x2y2)})

            if all(s['gt'] is None for s in _sequence_cins):
                if any(s['pd'] is not None for s in _sequence_cins):
                    _sequence_cin_fp += 1
                else:
                    _sequence_cin_tn += 1
            else:
                if any(s['cin'] for s in _sequence_cins if s['cin'] is not None):
                    _sequence_cin_tp += 1
                else:
                    _sequence_cin_fn += 1
        _sequence_cin_ap, _sequence_cin_ar = 0, 0
        if _sequence_cin_tp + _sequence_cin_fp != 0:
            _sequence_cin_ap = _sequence_cin_tp / (_sequence_cin_tp + _sequence_cin_fp)
        else:
            logger.warning("encountered division by zero when calculating _sequence_cin_ap. Set result to zero.")
        if _sequence_cin_tp + _sequence_cin_fn != 0:
            _sequence_cin_ar = _sequence_cin_tp / (_sequence_cin_tp + _sequence_cin_fn)
        else:
            logger.warning("encountered division by zero when calculating _sequence_cin_ar. Set result to zero.")
        return _sequence_cin_ap, _sequence_cin_ar, _sequence_cin_tp, _sequence_cin_fp, _sequence_cin_fn, _sequence_cin_tn

    for i, _ in enumerate(result_dicts):
        pos = np.argmax(result_dicts[i]['scores'], axis=0) if result_dicts[i]['scores'] else None
        result_dicts[i]['scores'] = result_dicts[i]['scores'][pos] if pos is not None else None
        result_dicts[i]['pd_loc'] = result_dicts[i]['pd_loc'][pos] if pos is not None else None
    ap_cin, ar_cin = evaluate_center_in_iou()
    iou_thres_list = np.linspace(0, 1, 21)
    ap_list, ar_list = evaluate_iou()
    sequence_ap_list, sequence_ar_list, sequence_tp_list, sequence_fp_list, sequence_fn_list, sequence_tn_list = evaluate_sequence_iou()
    sequence_cin_ap, sequence_cin_ar, sequence_cin_tp, sequence_cin_fp, sequence_cin_fn, sequence_cin_tn = evaluate_sequence_cin()

    table = tabulate(
        [['Perf-AP', ap_cin, *ap_list],
         ['Perf-AR', ar_cin, *ar_list],
         ['Seq-AP', sequence_cin_ap, *sequence_ap_list],
         ['Seq-AR', sequence_cin_ar, *sequence_ar_list],
         ['Seq-TP', sequence_cin_tp, *sequence_tp_list],
         ['Seq-FP', sequence_cin_fp, *sequence_fp_list],
         ['Seq-FN', sequence_cin_fn, *sequence_fn_list],
         ['Seq-TN', sequence_cin_tn, *sequence_tn_list]],
        headers=['IOU', 'center_in', *['{:.2f}'.format(t) for t in iou_thres_list]],
        tablefmt="pipe",
        floatfmt=".3f",
        stralign="center",
        numalign="center",
    )

    logger.info(
        "Evaluation results -- Num images: {}\n{}".format(len(result_dicts), table))
    return ap_list, ar_list, ap_cin, ar_cin


def evaluate2(result_dicts):
    for i, d in enumerate(result_dicts):
        pos = int(np.argmax(result_dicts[i]['scores'], axis=0)) if result_dicts[i]['scores'] else None
        result_dicts[i]['scores'] = result_dicts[i]['scores'][pos] if pos is not None else None
        result_dicts[i]['pd_loc'] = result_dicts[i]['pd_loc'][pos] if pos is not None else None

    prob_score_thr_list = np.linspace(0.05, 0.95, 30)

    rows_img = []
    img_auc = roc_auc_score([d['gt_loc'] is not None for d in result_dicts],
                            [0 if d['scores'] is None else d['scores'] for d in result_dicts])
    for prob_score_thr in prob_score_thr_list:
        rows_img.append([prob_score_thr, *evaluate_image_classification(result_dicts, prob_score_thr)])

    table = tabulate(
        rows_img,
        headers=['Prob_thr', 'Acc', 'F1', 'AP', 'AR', 'Spec', 'Sens', 'TP', 'FP', 'TN', 'FN'],
        tablefmt="pipe",
        floatfmt=(".2f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", "d", "d", "d", "d"),
        stralign="center",
        numalign="center",
    )
    logger.info(
        "==== Evaluation of image classification ====\n"
        "Num images: {}, AUC: {}\n{}".format(len(result_dicts), img_auc, table))

    rows_seq = []
    sequences = set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts])
    # sequences = set([os.path.basename(d['file_name']).split("_frame", 1)[0] for d in result_dicts])

    seq_y_true = []
    seq_y_score = []
    for s in sequences:
        frames = [d for d in result_dicts if s in d['file_name']]
        seq_y_true.append(any([(f['gt_loc'] is not None) for f in frames]))
        seq_y_score.append(max([0] + [f['scores'] for f in frames if (f['pd_loc'] is not None)]))
    seq_auc = roc_auc_score(seq_y_true, seq_y_score)
    # auc1 = roc_auc_score(
    #     [any([d for d in result_dicts if (d['gt_loc'] is not None) and (s in d['file_name'])]) for s in sequences],
    #     [max([0] + [d['scores'] for d in result_dicts if (d['pd_loc'] is not None) and (s in d['file_name'])]) for s in
    #      sequences])
    sequence_dict_list = []
    rows_seq = []
    headers = ['Prob_thr', 'Acc', 'F1', 'AP', 'AR', 'Spec', 'Sens', 'J', 'TP', 'FP', 'TN', 'FN']
    for prob_score_thr in prob_score_thr_list:
        values = [prob_score_thr, *evaluate_sequence_classification(result_dicts, 'pd_loc', prob_score_thr)]
        rows_seq.append(values)
        sequence_dict_list.append(dict(zip(headers, values)))
    optimal_seq_row = max(sequence_dict_list, key=lambda x: x['J'])
    optimal_prob_thres = optimal_seq_row['Prob_thr']

    table = tabulate(
        rows_seq,
        headers=headers,
        tablefmt="pipe",
        floatfmt=(".2f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", ".3f", "d", "d", "d", "d"),
        stralign="center",
        numalign="center",
    )
    logger.info(
        "==== Evaluation of sequence classification ====\n"
        "Num sequences: {}, AUC: {}\n{}".format(len(sequences), seq_auc, table))

    rows_perf = []
    perf_dict_list = []
    perforations = [d for d in result_dicts if (d['gt_loc'] is not None)]
    headers = ['Prob_thr', 'APcin', 'ARcin', 'F1cin', 'TPcin', 'FPcin', 'FNcin', 'APiou50', 'ARiou50', 'F1iou50',
               'TPiou50', 'FPiou50', 'FNiou50']
    for prob_score_thr in prob_score_thr_list:
        values = [prob_score_thr, *evaluate_perf_localization_cin(result_dicts, 'pd_loc', prob_score_thr),
                  *evaluate_perf_localization_iou(result_dicts, 'pd_loc', prob_score_thr)]
        rows_perf.append(values)
        perf_dict_list.append(dict(zip(headers, values)))

    # Data to plot precision - recall curve
    precision = [d['APcin'] for d in perf_dict_list]
    recall = [d['ARcin'] for d in perf_dict_list]
    auc_precision_recall = auc(recall, precision)

    optimal_perf_dict = next((item for item in perf_dict_list if item["Prob_thr"] == optimal_prob_thres), False)
    table = tabulate(
        rows_perf,
        headers=headers,
        tablefmt="pipe",
        floatfmt=(".2f", ".3f", ".3f", ".3f", "d", "d", "d", ".3f", ".3f", ".3f", "d", "d", "d"),
        stralign="center",
        numalign="center",
    )
    logger.info(
        "Evaluation of perforation localization -- Num perforations: {}\n{}".format(len(perforations), table))
    seq_y = [any([d for d in result_dicts if (d['gt_loc'] is not None) and (s in d['file_name'])]) for s in sequences],
    seq_scores = [
        float(max([0] + [d['scores'] for d in result_dicts if (d['pd_loc'] is not None) and (s in d['file_name'])])) for
        s in
        sequences]

    sens95_row = min(sequence_dict_list, key=lambda x: abs(x['Spec']-0.95))
    spec95_row = min(sequence_dict_list, key=lambda x: abs(x['Sens']-0.95))
    performance_dict = {'prob_thres': optimal_prob_thres,
                        'AP_cin': optimal_perf_dict['APcin'],
                        'AR_cin': optimal_perf_dict['ARcin'],
                        'AUPRC': auc_precision_recall,
                        'AP50': optimal_perf_dict['APiou50'],
                        'AR50': optimal_perf_dict['ARiou50'],
                        'Spec': optimal_seq_row['Spec'],
                        'Sens': optimal_seq_row['Sens'],
                        'Sens95': (sens95_row['Sens'], sens95_row['Spec']),
                        'Spec95': (spec95_row['Spec'], spec95_row['Sens']),
                        'AUC': seq_auc}
    logger.info("Performance Summary: {}".format(performance_dict))
    return rows_perf, seq_y, seq_scores, sequence_dict_list, perf_dict_list, performance_dict
