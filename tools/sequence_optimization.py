import argparse
import logging
import os
import random
import re
import sys
from itertools import combinations
from pathlib import Path
import glob
import mmcv
import numpy as np
import pandas as pd
import torch
from natsort import natsorted
import json
from eval_utils import evaluate2
import copy

logger = logging.getLogger(__name__)


def cin_x1y1x2y2(b1, b2):
    b2_xc = (b2[0] + b2[2]) / 2
    b2_yc = (b2[1] + b2[3]) / 2
    b2_minx = min(b2[0], b2[2])
    b2_maxx = max(b2[0], b2[2])
    b2_miny = min(b2[1], b2[3])
    b2_maxy = max(b2[1], b2[3])

    b1_xc = (b1[0] + b1[2]) / 2
    b1_yc = (b1[1] + b1[3]) / 2
    b1_minx = min(b1[0], b1[2])
    b1_maxx = max(b1[0], b1[2])
    b1_miny = min(b1[1], b1[3])
    b1_maxy = max(b1[1], b1[3])

    if (b1_maxx >= b2_xc >= b1_minx) and (b1_maxy >= b2_yc >= b1_miny):
        return True
    if (b2_maxx >= b1_xc >= b2_minx) and (b2_maxy >= b1_yc >= b2_miny):
        return True
    return False


def optimize_sequence(args):
    logger.info("========== Overall performance evaluation ==============")
    result_pickle_paths = []
    fold_dirpaths = glob.glob(os.path.join(args.dir, 'fold*/'), recursive=False)
    for fold_dirpath in fold_dirpaths:
        result_pickle_path = os.path.join(fold_dirpath, 'results_max{}'.format(args.max_per_img), 'result_dicts.pkl')
        result_pickle_paths.append(result_pickle_path)
    result_pickle_paths.append(os.path.join(args.dir, 'overall_result_dicts_max{}.pkl'.format(args.max_per_img)))

    for result_pickle_path in result_pickle_paths:
        result_dicts = mmcv.load(result_pickle_path)
        for i, _ in enumerate(result_dicts):
            result_dicts[i]['frame_number'] = int(
                re.search('frame([0-9]+?).jpg', result_dicts[i]['file_name']).group(1))
            result_dicts[i]['sequence_name'] = os.path.basename(os.path.dirname(result_dicts[i]['file_name']))

        '''suppress overlapping boxes on the same frame'''
        for dict_idx, frame_dict in enumerate(result_dicts):
            indices_to_be_removed = set()
            if len(frame_dict['scores']) > 1:
                box_pairs = list(map(list, combinations(range(0, len(frame_dict['scores'])), 2)))
                for pair in box_pairs:
                    if cin_x1y1x2y2(frame_dict['pd_loc'][pair[0]], frame_dict['pd_loc'][pair[1]]):
                        indices_to_be_removed.add(
                            (pair[0], pair[1])[frame_dict['scores'][pair[0]] > frame_dict['scores'][pair[1]]])
            if indices_to_be_removed:
                result_dicts[dict_idx]['scores'] = [s for i, s in enumerate(frame_dict['scores']) if
                                                          i not in indices_to_be_removed]
                result_dicts[dict_idx]['pd_loc'] = [s for i, s in enumerate(frame_dict['pd_loc']) if
                                                          i not in indices_to_be_removed]

        sequence_names = natsorted(set([os.path.basename(os.path.dirname(d['file_name'])) for d in result_dicts]))
        optimized_result_dicts = []
        sequence_lens = []
        for sequence_idx, sequence_name in enumerate(sequence_names):
            logger.info(
                "======== {}/{}, sequence: {} ========".format(sequence_idx + 1, len(sequence_names), sequence_name))
            optimal_trajectory = []
            optimal_trajectory_score = 0
            optimal_trajectory_max_score = 0
            sequence_result_dicts = [d for d in result_dicts if d['sequence_name'] == sequence_name]
            sequence_result_dicts = sorted(sequence_result_dicts, key=lambda k: k['frame_number'])
            sequence_lens.append(len(sequence_result_dicts))
            logger.info("sequence len: {}".format(len(sequence_result_dicts)))
            trajectory_loc_index_ranges = [range(len(d['scores']) + 1) for d in
                                           sequence_result_dicts]

            '''collect valid trajectories'''
            sequence_result_dicts_as_start = copy.deepcopy(sequence_result_dicts)
            trajectories = []
            while sequence_result_dicts_as_start:
                if len(sequence_result_dicts_as_start) == 1:
                    for i, _ in enumerate(sequence_result_dicts_as_start[0]['pd_loc']):
                        box_dict = copy.deepcopy(sequence_result_dicts_as_start[0])
                        box_dict['pd_loc'] = [box_dict['pd_loc'][i]]
                        box_dict['scores'] = [box_dict['scores'][i]]
                    sequence_result_dicts_as_start = []
                    continue
                start_frame = sequence_result_dicts_as_start.pop(0)
                if len(start_frame['pd_loc']) == 0:
                    continue
                for i, _ in enumerate(start_frame['pd_loc']):
                    _start_frame = copy.deepcopy(start_frame)
                    _start_frame['pd_loc'] = [_start_frame['pd_loc'][i]]
                    trajectory = [_start_frame]
                    # start_box = start_frame_dict['pd_loc'][i]
                    following_frame_dicts = [copy.deepcopy(d) for d in sequence_result_dicts if d['frame_number'] > start_frame['frame_number']]
                    following_frame_dicts = sorted(following_frame_dicts, key=lambda k: k['frame_number'])
                    between_overlapping_box_frame_dicts = []
                    for following_frame in following_frame_dicts:
                        if len(following_frame['pd_loc']) == 0:
                            between_overlapping_box_frame_dicts.append(copy.deepcopy(following_frame))
                            continue
                        overlapping_box = None
                        overlapping_box_score = 0
                        for j, following_frame_box in enumerate(following_frame['pd_loc']):
                            if cin_x1y1x2y2(trajectory[-1]['pd_loc'][0], following_frame_box):
                                if following_frame['scores'][j] > overlapping_box_score:
                                    overlapping_box = copy.deepcopy(following_frame)
                                    overlapping_box['pd_loc'] = [overlapping_box['pd_loc'][j]]
                                    overlapping_box['scores'] = [overlapping_box['scores'][j]]
                                    overlapping_box_score = overlapping_box['scores'][0]
                        if overlapping_box is not None:
                            if between_overlapping_box_frame_dicts:
                                trajectory.extend(copy.deepcopy(between_overlapping_box_frame_dicts))
                                between_overlapping_box_frame_dicts = []
                            trajectory.append(copy.deepcopy(overlapping_box))
                            '''remove this box from traj starting list'''
                            d = next(item for item in sequence_result_dicts_as_start if item['frame_number'] == following_frame['frame_number'])
                            pd_box_index_to_remove = [idx for idx, loc in enumerate(d['pd_loc']) if all(torch.eq(loc, overlapping_box['pd_loc'][0]))]
                            d['pd_loc'] = [loc for idx, loc in enumerate(d['pd_loc']) if idx not in pd_box_index_to_remove]
                            d['scores'] = [loc for idx, loc in enumerate(d['scores']) if idx not in pd_box_index_to_remove]
                        else:
                            potential_between_overlapping_box_frame = copy.deepcopy(following_frame)
                            potential_between_overlapping_box_frame['pd_loc'] = []
                            potential_between_overlapping_box_frame['scores'] = []
                            between_overlapping_box_frame_dicts.append(potential_between_overlapping_box_frame)

                    trajectories.append(copy.deepcopy(trajectory))

            if not trajectories:
                optimized_result_dicts.extend(copy.deepcopy(sequence_result_dicts))
                continue
            for trajectory in trajectories:
                trajectory_duration = len(trajectory)
                traj_boxes = [d['pd_loc'][0] for d in trajectory if d['pd_loc']]
                traj_scores = [d['scores'][0] for d in trajectory if d['scores']]
                assert len(traj_boxes) == len(traj_scores), "length of boxes ({}) and scores({}) do not match!".format(len(traj_boxes), len(traj_scores))
                trajectory_accumulative_score = sum(traj_scores)
                trajectory_max_score = max(traj_scores)
                trajectory_mean_score = np.mean(traj_scores)
                trajectory_weighted_score = trajectory_duration * trajectory_accumulative_score
                # 2. whether to use weighted score, or just the accumulative score
                if trajectory_weighted_score > optimal_trajectory_score:
                    optimal_trajectory = trajectory
                    optimal_trajectory_max_score = trajectory_mean_score
                    optimal_trajectory_score = trajectory_weighted_score

            # 3. whether to create bboxes in the middle of a trajectory: no
            # for frame_idx, frame_dict in enumerate(optimal_trajectory):
            #     if frame_dict['pd_loc']:
            #         optimal_trajectory[frame_idx]['scores'] = [optimal_trajectory_max_score]

            # 3. whether to create bboxes in the middle of a trajectory: yes
            for frame_idx, frame_dict in enumerate(optimal_trajectory):
                if not frame_dict['pd_loc']:
                    prev_frame_dict = copy.deepcopy(optimal_trajectory[frame_idx-1])
                    if not prev_frame_dict['pd_loc']:
                        raise ValueError("Did not find prev frame with valid bbox. This should not happen!")
                    next_frame_dict = []
                    for next_frame in range(frame_idx+1, len(optimal_trajectory)):
                        if not optimal_trajectory[next_frame]['pd_loc']:
                            continue
                        next_frame_dict = copy.deepcopy(optimal_trajectory[next_frame])
                        break
                    if not any(next_frame_dict):
                        raise ValueError("Did not find next frame with valid bbox. This should not happen!")
                    optimal_trajectory[frame_idx]['pd_loc'] = [(g + h) / 2 for g, h in zip(prev_frame_dict['pd_loc'], next_frame_dict['pd_loc'])]
                optimal_trajectory[frame_idx]['scores'] = [optimal_trajectory_max_score]
            logger.info("Number of trajectories (before): {}, perforations(before): {}, perforations (after): {}"
                        "".format(len(trajectories), sum([len(t) for t in trajectories]), len(optimal_trajectory)))
            optimal_trajectory_frame_numbers = [d['frame_number'] for d in optimal_trajectory]
            for frame_idx, frame_dict in enumerate(sequence_result_dicts):
                if frame_dict['frame_number'] not in optimal_trajectory_frame_numbers:
                    _frame_dict = copy.deepcopy(frame_dict)
                    _frame_dict['pd_loc'] = []
                    _frame_dict['scores'] = []
                    optimized_result_dicts.append(copy.deepcopy(_frame_dict))
                else:
                    d = next(item for item in optimal_trajectory if item['frame_number'] == frame_dict['frame_number'])
                    optimized_result_dicts.append(copy.deepcopy(d))

        optimized_result_dicts_path = os.path.join(Path(result_pickle_path).parent, 'optimized_{}'.format(Path(result_pickle_path).name))
        mmcv.dump(optimized_result_dicts, optimized_result_dicts_path)
        _, _, _, seq_dict_list, perf_dict_list, performance_dict = evaluate2(mmcv.load(optimized_result_dicts_path))

        pd.DataFrame(seq_dict_list).to_csv(os.path.join(Path(result_pickle_path).parent, "optimized_seq_cls_results_max{}.csv".format(args.max_per_img)), index=False)
        pd.DataFrame(perf_dict_list).to_csv(os.path.join(Path(result_pickle_path).parent, "optimized_perf_det_results_max{}.csv".format(args.max_per_img)), index=False)
        json.dump(performance_dict, open(os.path.join(Path(result_pickle_path).parent, "optimized_performance_max{}.json".format(args.max_per_img)), 'w+'))

        logger.info("sequence length stats: min: {}, max: {}, mean: {}".format(
            min(sequence_lens), max(sequence_lens), np.mean(sequence_lens)))


def parse_args():
    """
    Argument parser for the main function
    """
    parser = argparse.ArgumentParser(description='Register a pair of images')
    parser.add_argument('--method', type=str, help='The chosen method and model')
    parser.add_argument('--dir', type=str, help='The dir to store all outputs')
    parser.add_argument('--max_per_img', type=int, default=1, help='Max number of detections per image in test')
    parser.add_argument('--seq_len', type=int, help='Sequence length of data loader for temporal models')

    return parser.parse_args()


if __name__ == '__main__':
    """Train and cross validate various methods on the perforation data"""
    random.seed(42)
    torch.multiprocessing.set_sharing_strategy('file_system')

    args = parse_args()

    folder_name = "{}_seqlen{}_cosinelr5e-04".format(args.method, args.seq_len)
    vars(args)['dir'] = os.path.join(args.dir, folder_name)

    log_filepath = '{}/main_sequence_optimization_max{}.log'.format(args.dir, args.max_per_img)
    Path(log_filepath).parent.mkdir(parents=True, exist_ok=True)
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        handlers=[logging.FileHandler(log_filepath, mode='w'),
                                  logging.StreamHandler(sys.stdout)])
    optimize_sequence(args)
    print("Done!")
