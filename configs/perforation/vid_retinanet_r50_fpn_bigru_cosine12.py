# The new config inherits a base config to highlight the necessary modification
_base_ = [
    '../_base_/models/retinanet_r50_fpn.py',
    '../_base_/datasets/perf_vid_detection.py',
    '../_base_/schedules/schedule_cosine.py',
    '../_base_/default_runtime.py'
]

# We also need to change the num_classes in head to match the dataset's annotation
model = dict(
    type='RetinaNetBGRU',
    bbox_head=dict(num_classes=1),
    test_cfg=dict(max_per_img=1))


# We can use the pre-trained Faster RCNN model to obtain higher performance
load_from = 'checkpoints/retinanet_r50_fpn_1x_coco_20200130-c2398f9e.pth'
