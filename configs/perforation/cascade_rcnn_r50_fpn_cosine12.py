# The new config inherits a base config to highlight the necessary modification
_base_ = [
    '../_base_/models/cascade_rcnn_r50_fpn.py',
    '../_base_/datasets/coco_detection.py',
    '../_base_/schedules/schedule_cosine.py',
    '../_base_/default_runtime.py'
]

# We also need to change the num_classes in head to match the dataset's annotation
model = dict(
    roi_head=dict(
        bbox_head=[  # only changed number of classes to 1
            dict(
                type='Shared2FCBBoxHead',
                in_channels=256,
                fc_out_channels=1024,
                roi_feat_size=7,
                num_classes=1,
                bbox_coder=dict(
                    type='DeltaXYWHBBoxCoder',
                    target_means=[0., 0., 0., 0.],
                    target_stds=[0.1, 0.1, 0.2, 0.2]),
                reg_class_agnostic=True,
                loss_cls=dict(
                    type='CrossEntropyLoss',
                    use_sigmoid=False,
                    loss_weight=1.0),
                reg_decoded_bbox=True,
                loss_bbox=dict(type='DistanceReLuIoULoss', loss_weight=1, beta=0.25)),
            dict(
                type='Shared2FCBBoxHead',
                in_channels=256,
                fc_out_channels=1024,
                roi_feat_size=7,
                num_classes=1,
                bbox_coder=dict(
                    type='DeltaXYWHBBoxCoder',
                    target_means=[0., 0., 0., 0.],
                    target_stds=[0.05, 0.05, 0.1, 0.1]),
                reg_class_agnostic=True,
                loss_cls=dict(
                    type='CrossEntropyLoss',
                    use_sigmoid=False,
                    loss_weight=1.0),
                reg_decoded_bbox=True,
                loss_bbox=dict(type='DistanceReLuIoULoss', loss_weight=1, beta=0.25)),
            dict(
                type='Shared2FCBBoxHead',
                in_channels=256,
                fc_out_channels=1024,
                roi_feat_size=7,
                num_classes=1,
                bbox_coder=dict(
                    type='DeltaXYWHBBoxCoder',
                    target_means=[0., 0., 0., 0.],
                    target_stds=[0.033, 0.033, 0.067, 0.067]),
                reg_class_agnostic=True,
                loss_cls=dict(
                    type='CrossEntropyLoss',
                    use_sigmoid=False,
                    loss_weight=1.0),
                reg_decoded_bbox=True,
                loss_bbox=dict(type='DistanceReLuIoULoss', loss_weight=1, beta=0.25))
        ]),
    # train_cfg=dict(
        # rpn=dict(
        #     sampler=dict(
        #         neg_pos_ub=5)),
        # rcnn=[
        #     dict(
        #         assigner=dict(
        #             type='MaxIoUAssigner',
        #             pos_iou_thr=0.5,
        #             neg_iou_thr=0.5,
        #             min_pos_iou=0.5,
        #             match_low_quality=False,
        #             ignore_iof_thr=-1),
        #         sampler=dict(
        #             type='RandomSampler',
        #             num=512,
        #             pos_fraction=0.25,
        #             neg_pos_ub=5,
        #             add_gt_as_proposals=True),
        #         mask_size=28,
        #         pos_weight=-1,
        #         debug=False),
        #     dict(
        #         assigner=dict(
        #             type='MaxIoUAssigner',
        #             pos_iou_thr=0.6,
        #             neg_iou_thr=0.6,
        #             min_pos_iou=0.6,
        #             match_low_quality=False,
        #             ignore_iof_thr=-1),
        #         sampler=dict(
        #             type='RandomSampler',
        #             num=512,
        #             pos_fraction=0.25,
        #             neg_pos_ub=5,
        #             add_gt_as_proposals=True),
        #         mask_size=28,
        #         pos_weight=-1,
        #         debug=False),
        #     dict(
        #         assigner=dict(
        #             type='MaxIoUAssigner',
        #             pos_iou_thr=0.7,
        #             neg_iou_thr=0.7,
        #             min_pos_iou=0.7,
        #             match_low_quality=False,
        #             ignore_iof_thr=-1),
        #         sampler=dict(
        #             type='RandomSampler',
        #             num=512,
        #             pos_fraction=0.25,
        #             neg_pos_ub=5,
        #             add_gt_as_proposals=True),
        #         mask_size=28,
        #         pos_weight=-1,
        #         debug=False)
        # ]),
    test_cfg=dict(
        rcnn=dict(
            max_per_img=1)))

# Modify dataset related settings
dataset_type = 'PerfVIDDataset'
classes = ('perforation',)
data = dict(
    train=dict(
        img_prefix='',
        classes=classes,
        ann_file='data/perforation/json/annotation_coco.json'),
    val=dict(
        img_prefix='',
        classes=classes,
        ann_file='data/perforation/json/annotation_coco.json'),
    test=dict(
        img_prefix='',
        classes=classes,
        ann_file='data/perforation/json/annotation_coco.json'))

# We can use the pre-trained Faster RCNN model to obtain higher performance
load_from = 'checkpoints/cascade_rcnn_r50_fpn_1x_coco_20200316-3dc56deb.pth'
