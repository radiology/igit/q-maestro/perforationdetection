dataset_type = 'PerfVIDDataset'
classes = ('perforation',)
data_root = 'data/coco/'
img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True)

albu_train_transforms = [
    dict(
        type='ShiftScaleRotate',
        shift_limit=0.0625,
        scale_limit=0.1,
        rotate_limit=10,
        border_mode=1,
        p=0.5)
]

train_pipeline = [
    dict(type='LoadMultiChannelImageFromFiles', color_type='grayscale'),
    dict(type='LoadAnnotations', with_bbox=True),
    dict(type='Resize', img_scale=(640, 640), keep_ratio=True),
    dict(type='Pad', size_divisor=32),
    dict(type='RandomFlip', flip_ratio=0.5),
    dict(
        type='Albu',
        transforms=albu_train_transforms,
        bbox_params=dict(
            type='BboxParams',
            format='pascal_voc',
            label_fields=['gt_labels'],
            min_area=64,
            min_visibility=0.5,
            filter_lost_elements=True),
        keymap={
            'img': 'image',
            'gt_bboxes': 'bboxes'
        },
        update_pad_shape=False,
        skip_img_without_anno=False),
    dict(type='MultiFrameAddRGBChannel'),
    dict(type='Normalize', **img_norm_cfg),
    dict(type='DefaultFormatBundle'),
    dict(type='Collect', keys=['img', 'gt_bboxes', 'gt_labels']),
]
test_pipeline = [
    dict(type='LoadMultiChannelImageFromFiles'),
    dict(
        type='MultiScaleFlipAug',
        img_scale=(640, 640),
        flip=False,
        transforms=[
            dict(type='Resize', keep_ratio=True),
            dict(type='RandomFlip'),
            dict(type='Normalize', **img_norm_cfg),
            dict(type='Pad', size_divisor=32),
            dict(type='ImageToTensor', keys=['img']),
            dict(type='Collect', keys=['img']),
        ])
]
seq_len = 5
data = dict(
    samples_per_gpu=2,
    workers_per_gpu=2,
    train=dict(
        seq_len=seq_len,
        classes=classes,
        type=dataset_type,
        ann_file='data/perforation/json/annotation_coco.json',
        img_prefix='',
        pipeline=train_pipeline),
    val=dict(
        seq_len=seq_len,
        classes=classes,
        type=dataset_type,
        ann_file='data/perforation/json/annotation_coco.json',
        img_prefix='',
        pipeline=test_pipeline),
    test=dict(
        seq_len=seq_len,
        classes=classes,
        type=dataset_type,
        ann_file='data/perforation/json/annotation_coco.json',
        img_prefix='',
        pipeline=test_pipeline))
evaluation = dict(interval=1, metric='bbox')
